﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyecto.Functions
{
    class Utils
    {
        private string cadena = "server = 127.0.0.1; database = turist_chile; Uid = root; password=''";
        // ============= Asignar =============
        public void asignarSede(int idTrabajador, int idSede)
        {
            try
            {
                using (MySqlConnection conexionBD = new MySqlConnection(cadena))
                {
                    conexionBD.Open();

                    using (MySqlCommand cmd = new MySqlCommand("insert into trabajadores_sedes(id_trabajador, id_sede) values (@1, @2)", conexionBD))
                    {
                        cmd.Parameters.AddWithValue("@1", idTrabajador);
                        cmd.Parameters.AddWithValue("@2", idSede);

                        int estado = cmd.ExecuteNonQuery();
                        if (estado >= 1)
                        {
                            conexionBD.Close();

                            MessageBox.Show("Asignado correctamente", "Hecho", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        conexionBD.Close();
                    }
                    conexionBD.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        //  ============= Get ==================
        public int getIdRegion(string nombre)
        {
            int idRegion = 0;
            try
            {
                using (MySqlConnection conexionBD = new MySqlConnection(cadena))
                {
                    conexionBD.Open();
                    using (MySqlCommand cmd = new MySqlCommand("SELECT * FROM regiones Where nombre=@1", conexionBD))
                    {
                        cmd.Parameters.AddWithValue("@1", nombre);
                        MySqlDataReader dataReader = cmd.ExecuteReader();

                        while (dataReader.Read())
                        {
                            idRegion = Convert.ToInt32(dataReader["id"]);
                        }

                        conexionBD.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return idRegion;
        }
        public int getIdCiudad(string nombre)
        {
            int idCiudad = 0;
            try
            {
                using (MySqlConnection conexionBD = new MySqlConnection(cadena))
                {
                    conexionBD.Open();
                    using (MySqlCommand cmd = new MySqlCommand("SELECT * FROM ciudades Where nombre=@1", conexionBD))
                    {
                        cmd.Parameters.AddWithValue("@1", nombre);
                        MySqlDataReader dataReader = cmd.ExecuteReader();

                        while (dataReader.Read())
                        {
                            idCiudad = Convert.ToInt32(dataReader["id"]);
                        }

                        conexionBD.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return idCiudad;
        }
        public int getIdComuna(string nombre)
        {
            int idComuna = 0;
            try
            {
                using (MySqlConnection conexionBD = new MySqlConnection(cadena))
                {
                    conexionBD.Open();
                    using (MySqlCommand cmd = new MySqlCommand("SELECT * FROM comunas Where nombre=@1", conexionBD))
                    {
                        cmd.Parameters.AddWithValue("@1", nombre);
                        MySqlDataReader dataReader = cmd.ExecuteReader();

                        while (dataReader.Read())
                        {
                            idComuna = Convert.ToInt32(dataReader["id"]);
                        }

                        conexionBD.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return idComuna;
        }
        public int getIdPersona(string rut)
        {
            int idPersona = 0;
            try
            {
                using (MySqlConnection conexionBD = new MySqlConnection(cadena))
                {
                    conexionBD.Open();
                    using (MySqlCommand cmd = new MySqlCommand("SELECT * FROM personas Where rut=@1", conexionBD))
                    {
                        cmd.Parameters.AddWithValue("@1", rut);
                        MySqlDataReader dataReader = cmd.ExecuteReader();

                        while (dataReader.Read())
                        {
                            idPersona = Convert.ToInt32(dataReader["id"]);
                        }

                        conexionBD.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return idPersona;
        }
        public int getIdTour(string nombre)
        {
            int idPersona = 0;
            try
            {
                using (MySqlConnection conexionBD = new MySqlConnection(cadena))
                {
                    conexionBD.Open();
                    using (MySqlCommand cmd = new MySqlCommand("SELECT * FROM tours Where nombre=@1", conexionBD))
                    {
                        cmd.Parameters.AddWithValue("@1", nombre);
                        MySqlDataReader dataReader = cmd.ExecuteReader();

                        while (dataReader.Read())
                        {
                            idPersona = Convert.ToInt32(dataReader["id"]);
                        }

                        conexionBD.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return idPersona;
        }
        public int getIdJornada(string nombre)
        {
            int idJornada = 0;
            try
            {
                using (MySqlConnection conexionBD = new MySqlConnection(cadena))
                {
                    conexionBD.Open();
                    using (MySqlCommand cmd = new MySqlCommand("SELECT * FROM jornadas Where nombre=@1", conexionBD))
                    {
                        cmd.Parameters.AddWithValue("@1", nombre);
                        MySqlDataReader dataReader = cmd.ExecuteReader();

                        while (dataReader.Read())
                        {
                            idJornada = Convert.ToInt32(dataReader["id"]);
                        }

                        conexionBD.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return idJornada;
        }
        public int getIdTrabajador(string rut)
        {
            int idPersona = getIdPersona(rut);
            int idTrabajador = 0;
            try
            {
                using (MySqlConnection conexionBD = new MySqlConnection(cadena))
                {
                    conexionBD.Open();
                    using (MySqlCommand cmd = new MySqlCommand("SELECT * FROM trabajadores Where id_persona=@1", conexionBD))
                    {
                        cmd.Parameters.AddWithValue("@1", idPersona);
                        MySqlDataReader dataReader = cmd.ExecuteReader();

                        while (dataReader.Read())
                        {
                            idTrabajador = Convert.ToInt32(dataReader["id"]);
                        }

                        conexionBD.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return idTrabajador;
        }
        public int getIdSede(string nombre)
        {
            int idSede = 0;
            try
            {
                using (MySqlConnection conexionBD = new MySqlConnection(cadena))
                {
                    conexionBD.Open();
                    using (MySqlCommand cmd = new MySqlCommand("SELECT * FROM sedes Where nombre=@1", conexionBD))
                    {
                        cmd.Parameters.AddWithValue("@1", nombre);
                        MySqlDataReader dataReader = cmd.ExecuteReader();

                        while (dataReader.Read())
                        {
                            idSede = Convert.ToInt32(dataReader["id"]);
                        }

                        conexionBD.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return idSede;
        }
        //  ================ Cargar ================
        public void cargarTour(ComboBox combo)
        {
            try
            {
                using (MySqlConnection conexionBD = new MySqlConnection(cadena))
                {
                    conexionBD.Open();
                    using (MySqlCommand cmd = new MySqlCommand("SELECT * FROM tours", conexionBD))
                    {

                        MySqlDataReader dataReader = cmd.ExecuteReader();

                        while (dataReader.Read())
                        {
                            combo.Items.Add(dataReader["nombre"].ToString());
                        }

                        conexionBD.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void cargarSedes(ComboBox combo)
        {
            try
            {
                using (MySqlConnection conexionBD = new MySqlConnection(cadena))
                {
                    conexionBD.Open();
                    using (MySqlCommand cmd = new MySqlCommand("SELECT * FROM sedes", conexionBD))
                    {

                        MySqlDataReader dataReader = cmd.ExecuteReader();

                        while (dataReader.Read())
                        {
                            combo.Items.Add(dataReader["nombre"].ToString());
                        }

                        conexionBD.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void cargarTrabajadores(ComboBox combo)
        {
            try
            {
                using (MySqlConnection conexionBD = new MySqlConnection(cadena))
                {
                    conexionBD.Open();
                    using (MySqlCommand cmd = new MySqlCommand("SELECT * FROM personas", conexionBD))
                    {

                        MySqlDataReader dataReader = cmd.ExecuteReader();

                        while (dataReader.Read())
                        {
                            combo.Items.Add(dataReader["rut"].ToString());
                        }

                        conexionBD.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void cargarTrabajadoresHabilitados(ComboBox combo)
        {
            try
            {
                using (MySqlConnection conexionBD = new MySqlConnection(cadena))
                {
                    conexionBD.Open();
                    using (MySqlCommand cmd = new MySqlCommand("SELECT * FROM personas", conexionBD))
                    {

                        MySqlDataReader dataReader = cmd.ExecuteReader();

                        while (dataReader.Read())
                        {
                            string rut = dataReader["rut"].ToString();
                            if (VerificarTrabajadorHabilitado(rut))
                            {
                                combo.Items.Add(rut);
                            }
                        }
                        conexionBD.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        //  comboJornada
        public void cargarJornadas(ComboBox combo)
        {
            try
            {
                using (MySqlConnection conexionBD = new MySqlConnection(cadena))
                {
                    conexionBD.Open();
                    using (MySqlCommand cmd = new MySqlCommand("SELECT * FROM jornadas", conexionBD))
                    {
                        MySqlDataReader dataReader = cmd.ExecuteReader();
                        while (dataReader.Read())
                        {
                            combo.Items.Add(dataReader["nombre"].ToString());
                        }
                        conexionBD.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void cargarRegiones(ComboBox combo)
        {
            try
            {
                using (MySqlConnection conexionBD = new MySqlConnection(cadena))
                {
                    conexionBD.Open();
                    using (MySqlCommand cmd = new MySqlCommand("SELECT * FROM regiones", conexionBD))
                    {

                        MySqlDataReader dataReader = cmd.ExecuteReader();

                        while (dataReader.Read())
                        {
                            combo.Items.Add(dataReader["nombre"].ToString());
                        }

                        conexionBD.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void cargarCiudades(ComboBox combo)
        {
            try
            {
                using (MySqlConnection conexionBD = new MySqlConnection(cadena))
                {
                    conexionBD.Open();
                    using (MySqlCommand cmd = new MySqlCommand("SELECT * FROM ciudades", conexionBD))
                    {

                        MySqlDataReader dataReader = cmd.ExecuteReader();

                        while (dataReader.Read())
                        {
                            combo.Items.Add(dataReader["nombre"].ToString());
                        }

                        conexionBD.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void cargarComunas(ComboBox combo)
        {
            try
            {
                using (MySqlConnection conexionBD = new MySqlConnection(cadena))
                {
                    conexionBD.Open();
                    using (MySqlCommand cmd = new MySqlCommand("SELECT * FROM comunas", conexionBD))
                    {

                        MySqlDataReader dataReader = cmd.ExecuteReader();

                        while (dataReader.Read())
                        {
                            combo.Items.Add(dataReader["nombre"].ToString());
                        }

                        conexionBD.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        //  Crear
        public void crearTrabajadorTour(int idTrabajador, int idTour)
        {
            try
            {
                using (MySqlConnection conexionBD = new MySqlConnection(cadena))
                {
                    conexionBD.Open();

                    using (MySqlCommand cmd = new MySqlCommand("insert into trabajadores_tours(id_trabajador, id_tour) values (@1, @2)", conexionBD))
                    {
                        cmd.Parameters.AddWithValue("@1", idTrabajador);
                        cmd.Parameters.AddWithValue("@2", idTour);

                        int estado = cmd.ExecuteNonQuery();
                        if (estado >= 1)
                        {
                            conexionBD.Close();

                            MessageBox.Show("Trabajador agregado exitosamente", "Hecho", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        }

                        conexionBD.Close();
                    }
                    conexionBD.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        public void crearRegion(ComboBox combo)
        {
            try
            {
                using (MySqlConnection conexionBD = new MySqlConnection(cadena))
                {
                    conexionBD.Open();

                    using (MySqlCommand cmd = new MySqlCommand("insert into regiones(nombre) values (@1)", conexionBD))
                    {
                        cmd.Parameters.AddWithValue("@1", combo.SelectedItem.ToString());

                        int estado = cmd.ExecuteNonQuery();
                        if (estado >= 1)
                        {
                            conexionBD.Close();

                            MessageBox.Show("Region creada exitosamente", "Hecho", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        }

                        conexionBD.Close();
                    }
                    conexionBD.Close();
                }
            }
            catch (Exception ex)
            {
                if (ex.ToString().Contains("Duplicate entry"))
                {
                    MessageBox.Show("La region ya existe", "Regus", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }
        //  Combociudad
        public void crearCiudad(int idRegion, ComboBox combo)
        {
            try
            {
                using (MySqlConnection conexionBD = new MySqlConnection(cadena))
                {
                    conexionBD.Open();

                    using (MySqlCommand cmd = new MySqlCommand("insert into ciudades(nombre, id_region) values (@1, @2)", conexionBD))
                    {
                        cmd.Parameters.AddWithValue("@1", combo.SelectedItem.ToString());
                        cmd.Parameters.AddWithValue("@2", idRegion);

                        int estado = cmd.ExecuteNonQuery();
                        if (estado >= 1)
                        {
                            conexionBD.Close();

                            MessageBox.Show("Ciudad creada exitosamente", "Hecho", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        }

                        conexionBD.Close();
                    }
                    conexionBD.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        public void crearComunas(int idCiudad, ComboBox combo)
        {
            try
            {
                using (MySqlConnection conexionBD = new MySqlConnection(cadena))
                {
                    conexionBD.Open();

                    using (MySqlCommand cmd = new MySqlCommand("insert into comunas(nombre, id_ciudad) values (@1, @2)", conexionBD))
                    {
                        cmd.Parameters.AddWithValue("@1", combo.SelectedItem.ToString());
                        cmd.Parameters.AddWithValue("@2", idCiudad);

                        int estado = cmd.ExecuteNonQuery();
                        if (estado >= 1)
                        {
                            conexionBD.Close();

                            MessageBox.Show("Comuna creada exitosamente", "Hecho", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        }

                        conexionBD.Close();
                    }
                    conexionBD.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        public void crearSede(int idComuna, string nombre)
        {
            try
            {
                using (MySqlConnection conexionBD = new MySqlConnection(cadena))
                {
                    conexionBD.Open();

                    using (MySqlCommand cmd = new MySqlCommand("insert into sedes(nombre, id_comuna) values (@1, @2)", conexionBD))
                    {
                        cmd.Parameters.AddWithValue("@1", nombre);
                        cmd.Parameters.AddWithValue("@2", idComuna);

                        int estado = cmd.ExecuteNonQuery();
                        if (estado >= 1)
                        {
                            conexionBD.Close();

                            MessageBox.Show("Sede creada exitosamente", "Hecho", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        }

                        conexionBD.Close();
                    }
                    conexionBD.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        public void crearTour(string nombre, string descripcion, int precio, int id)
        {
            try
            {
                using (MySqlConnection conexionBD = new MySqlConnection(cadena))
                {
                    conexionBD.Open();

                    using (MySqlCommand cmd = new MySqlCommand("insert into tours(nombre, descripcion, precio, id_sede) values (@1, @2, @3, @4)", conexionBD))
                    {
                        cmd.Parameters.AddWithValue("@1", nombre);
                        cmd.Parameters.AddWithValue("@2", descripcion);
                        cmd.Parameters.AddWithValue("@3", precio);
                        cmd.Parameters.AddWithValue("@4", id);

                        int estado = cmd.ExecuteNonQuery();
                        if (estado >= 1)
                        {
                            conexionBD.Close();

                            MessageBox.Show("Tour creado exitosamente", "Hecho", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        }

                        conexionBD.Close();
                    }
                    conexionBD.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        public void crearHorario(string diahora, int cupos, int id_tour)
        {
            try
            {
                using (MySqlConnection conexionBD = new MySqlConnection(cadena))
                {
                    conexionBD.Open();

                    using (MySqlCommand cmd = new MySqlCommand("insert into horarios(dia_hora, cupos, id_tour) values (@1, @2, @3)", conexionBD))
                    {
                        cmd.Parameters.AddWithValue("@1", diahora);
                        cmd.Parameters.AddWithValue("@2", cupos);
                        cmd.Parameters.AddWithValue("@3", id_tour);

                        int estado = cmd.ExecuteNonQuery();
                        if (estado >= 1)
                        {
                            conexionBD.Close();

                            MessageBox.Show("Horario agregado correctamente", "Hecho", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        }

                        conexionBD.Close();
                    }
                    conexionBD.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        public void crearFormasPagos(string nombre)
        {
            try
            {
                using (MySqlConnection conexionBD = new MySqlConnection(cadena))
                {
                    conexionBD.Open();

                    using (MySqlCommand cmd = new MySqlCommand("insert into formas_pagos(nombre) values (@1)", conexionBD))
                    {
                        cmd.Parameters.AddWithValue("@1", nombre);

                        int estado = cmd.ExecuteNonQuery();
                        if (estado >= 1)
                        {
                            conexionBD.Close();

                            MessageBox.Show("Forma de pago agregada correctamente", "Hecho", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        }

                        conexionBD.Close();
                    }
                    conexionBD.Close();
                }
            }
            catch (Exception ex)
            {
                if (ex.ToString().Contains("Duplicate entry"))
                {
                    MessageBox.Show("La forma de pago ya existe", "Registro duplicado", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }
        //  ============== Verificar ================
        public bool VerificarTrabajadorHabilitado(string rut)
        {
            int idPersona = getIdPersona(rut);
            try
            {
                using (MySqlConnection conexionBD = new MySqlConnection(cadena))
                {
                    conexionBD.Open();
                    using (MySqlCommand cmd = new MySqlCommand("SELECT * FROM trabajadores WHERE id_persona=@1", conexionBD))
                    {
                        cmd.Parameters.AddWithValue("@1", idPersona);
                        MySqlDataReader dataReader;

                        dataReader = cmd.ExecuteReader();
                        dataReader.Read();
                        if (dataReader.HasRows == true)
                        {
                            return true;
                        }
                        if (dataReader.HasRows == false)
                        {
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return false;
        }
        public bool VerificarDuplicadoTrabajadoresTour(int idTrabajador, int idTour)
        {
            try
            {
                using (MySqlConnection conexionBD = new MySqlConnection(cadena))
                {
                    conexionBD.Open();
                    using (MySqlCommand cmd = new MySqlCommand("SELECT * FROM trabajadores_tours WHERE id_trabajador=@1 AND id_tour=@2", conexionBD))
                    {
                        cmd.Parameters.AddWithValue("@1", idTrabajador);
                        cmd.Parameters.AddWithValue("@2", idTour);
                        MySqlDataReader dataReader;

                        dataReader = cmd.ExecuteReader();
                        dataReader.Read();
                        if (dataReader.HasRows == true)
                        {
                            return false;
                        }
                        if (dataReader.HasRows == false)
                        {
                            return true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return false;
        }
        public bool VerificarDuplicadoTrabajadoresSedes(int idTrabajador, int idSede)
        {
            try
            {
                using (MySqlConnection conexionBD = new MySqlConnection(cadena))
                {
                    conexionBD.Open();
                    using (MySqlCommand cmd = new MySqlCommand("SELECT * FROM trabajadores_sedes WHERE id_trabajador=@1 AND id_sede=@2", conexionBD))
                    {
                        cmd.Parameters.AddWithValue("@1", idTrabajador);
                        cmd.Parameters.AddWithValue("@2", idSede);
                        MySqlDataReader dataReader;

                        dataReader = cmd.ExecuteReader();
                        dataReader.Read();
                        if (dataReader.HasRows == true)
                        {
                            return false;
                        }
                        if (dataReader.HasRows == false)
                        {
                            return true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return false;
        }
    }
}
