﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto.Functions
{
    class Verifications
    {
        public void formatearNumeros(System.Windows.Forms.TextBox textBox)
        {
            long numero = 0;
            bool esNumero = long.TryParse(textBox.Text, out numero);
            if (textBox.Text != "" && textBox.Text != null)
            {
                if (esNumero == true)
                {
                    numero = Convert.ToInt64(textBox.Text);
                }
                if (esNumero == false)
                {
                    //int errorCounter = Regex.Matches(textBox.Text, @"[a-zA-Z]").Count;

                    if (textBox.Text.Contains(".") == true || textBox.Text.Contains(",") == true)
                    {
                        textBox.Text = textBox.Text.Replace(".", string.Empty);
                        textBox.Text = textBox.Text.Replace(",", string.Empty);
                    }
                    numero = Convert.ToInt64(textBox.Text);
                }
                //txtPrecioCompra.Text = String.Format("{0:n0}", numero).ToString(new CultureInfo("is-IS"));
                //txtPrecioCompra.Text = numero.ToString("N", new CultureInfo("is-IS"));
                textBox.Text = numero.ToString("#,#", new CultureInfo("is-IS"));

            }
        }
        public string formatearNumerosString(string numeroC)
        {
            long numero = 0;
            bool esNumero = long.TryParse(numeroC, out numero);
            if (numeroC != "" && numeroC != null)
            {
                if (esNumero == true)
                {
                    numero = Convert.ToInt64(numeroC);
                    return numero.ToString("#,#", new CultureInfo("is-IS"));
                }
                if (esNumero == false)
                {
                    if (numeroC.Contains(".") == true || numeroC.Contains(",") == true)
                    {
                        numeroC = numeroC.Replace(".", string.Empty);
                        numeroC = numeroC.Replace(",", string.Empty);
                    }
                    numero = Convert.ToInt64(numeroC);
                    return numero.ToString("#,#", new CultureInfo("is-IS"));
                }
                //txtPrecioCompra.Text = String.Format("{0:n0}", numero).ToString(new CultureInfo("is-IS"));
                //txtPrecioCompra.Text = numero.ToString("N", new CultureInfo("is-IS"));
                return numero.ToString("#,#", new CultureInfo("is-IS"));
            }
            return numero.ToString("#,#", new CultureInfo("is-IS"));
        }
    }
}
