﻿using MySql.Data.MySqlClient;
using Proyecto.Functions;
using Proyecto.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyecto
{
    public partial class LoginForm : Form
    {
        
        private string connectionString = string.Empty;
        public LoginForm()
        {
            InitializeComponent();
            DBUtils db = new DBUtils();
            connectionString = db.returnConnectionString();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (txtEmail.Text == "" || txtEmail.Text == null) {
                MessageBox.Show("El correo no puede quedar vacio", "Campo vacio", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            if (txtPass.Text == "" || txtPass.Text == null)
            {
                MessageBox.Show("La contraseña no puede quedar vacio", "Campo vacio", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            if (txtEmail.Text != string.Empty && txtPass.Text != string.Empty)
            {
                using (MySqlConnection conexionBD = new MySqlConnection(connectionString))
                {
                    conexionBD.Open();
                    using (MySqlCommand cmd = new MySqlCommand("SELECT * FROM personas WHERE email=@1 AND contrasena=@2", conexionBD))
                    {
                        cmd.Parameters.AddWithValue("@1", txtEmail.Text);
                        cmd.Parameters.AddWithValue("@2", txtPass.Text);

                        MySqlDataReader dataReader;

                        dataReader = cmd.ExecuteReader();
                        dataReader.Read();
                        if (dataReader.HasRows == true)
                        {
                            conexionBD.Close();
                            MainForm Mf = new MainForm(txtEmail.Text, txtPass.Text);
                            Mf.Show();
                            this.Hide();
                            return;
                        }
                        if (dataReader.HasRows == false)
                        {
                            conexionBD.Close();
                            MessageBox.Show("Usuario o contraseña invalidos\nRevise los datos e intentelo otra vez");
                            return;
                        }
                        conexionBD.Close();
                    }
                }
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }

        private void bunifuGradientPanel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
