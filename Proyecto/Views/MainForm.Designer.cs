﻿
namespace Proyecto.Views
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges borderEdges1 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties1 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties2 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            this.mainBar = new System.Windows.Forms.Panel();
            this.btnClose = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.labelNombre = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel2 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel1 = new Bunifu.UI.WinForms.BunifuLabel();
            this.labelCorreo = new Bunifu.UI.WinForms.BunifuLabel();
            this.dragPanel = new System.Windows.Forms.Panel();
            this.navBar = new System.Windows.Forms.Panel();
            this.agregarTour = new System.Windows.Forms.Button();
            this.btnAgregarSede = new System.Windows.Forms.Button();
            this.btnAgregarTrabajador = new System.Windows.Forms.Button();
            this.btnCrearJornada = new System.Windows.Forms.Button();
            this.contentPanel = new System.Windows.Forms.Panel();
            this.bunifuElipse1 = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.bunifuDragControl1 = new Bunifu.Framework.UI.BunifuDragControl(this.components);
            this.agregarFormas = new System.Windows.Forms.Button();
            this.mainBar.SuspendLayout();
            this.navBar.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainBar
            // 
            this.mainBar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(138)))), ((int)(((byte)(7)))));
            this.mainBar.Controls.Add(this.btnClose);
            this.mainBar.Controls.Add(this.labelNombre);
            this.mainBar.Controls.Add(this.bunifuLabel2);
            this.mainBar.Controls.Add(this.bunifuLabel1);
            this.mainBar.Controls.Add(this.labelCorreo);
            this.mainBar.Controls.Add(this.dragPanel);
            this.mainBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.mainBar.Location = new System.Drawing.Point(0, 0);
            this.mainBar.Name = "mainBar";
            this.mainBar.Size = new System.Drawing.Size(995, 97);
            this.mainBar.TabIndex = 0;
            // 
            // btnClose
            // 
            this.btnClose.AllowToggling = false;
            this.btnClose.AnimationSpeed = 200;
            this.btnClose.AutoGenerateColors = false;
            this.btnClose.BackColor = System.Drawing.Color.Transparent;
            this.btnClose.BackColor1 = System.Drawing.Color.Transparent;
            this.btnClose.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnClose.BackgroundImage")));
            this.btnClose.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.btnClose.ButtonText = "";
            this.btnClose.ButtonTextMarginLeft = 0;
            this.btnClose.ColorContrastOnClick = 45;
            this.btnClose.ColorContrastOnHover = 45;
            this.btnClose.Cursor = System.Windows.Forms.Cursors.Hand;
            borderEdges1.BottomLeft = true;
            borderEdges1.BottomRight = true;
            borderEdges1.TopLeft = true;
            borderEdges1.TopRight = true;
            this.btnClose.CustomizableEdges = borderEdges1;
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnClose.DisabledBorderColor = System.Drawing.Color.Empty;
            this.btnClose.DisabledFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.btnClose.DisabledForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.btnClose.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed;
            this.btnClose.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F);
            this.btnClose.ForeColor = System.Drawing.Color.White;
            this.btnClose.IconLeftCursor = System.Windows.Forms.Cursors.Hand;
            this.btnClose.IconMarginLeft = 11;
            this.btnClose.IconPadding = 10;
            this.btnClose.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.btnClose.IdleBorderColor = System.Drawing.Color.Black;
            this.btnClose.IdleBorderRadius = 5;
            this.btnClose.IdleBorderThickness = 2;
            this.btnClose.IdleFillColor = System.Drawing.Color.Transparent;
            this.btnClose.IdleIconLeftImage = global::Proyecto.Properties.Resources.cerrar;
            this.btnClose.IdleIconRightImage = null;
            this.btnClose.IndicateFocus = false;
            this.btnClose.Location = new System.Drawing.Point(952, 2);
            this.btnClose.Name = "btnClose";
            stateProperties1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties1.BorderRadius = 5;
            stateProperties1.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties1.BorderThickness = 2;
            stateProperties1.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties1.ForeColor = System.Drawing.Color.White;
            stateProperties1.IconLeftImage = null;
            stateProperties1.IconRightImage = null;
            this.btnClose.onHoverState = stateProperties1;
            stateProperties2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties2.BorderRadius = 5;
            stateProperties2.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties2.BorderThickness = 2;
            stateProperties2.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties2.ForeColor = System.Drawing.Color.White;
            stateProperties2.IconLeftImage = null;
            stateProperties2.IconRightImage = null;
            this.btnClose.OnPressedState = stateProperties2;
            this.btnClose.Size = new System.Drawing.Size(40, 40);
            this.btnClose.TabIndex = 4;
            this.btnClose.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnClose.TextMarginLeft = 0;
            this.btnClose.UseDefaultRadiusAndThickness = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // labelNombre
            // 
            this.labelNombre.AutoEllipsis = false;
            this.labelNombre.CursorType = null;
            this.labelNombre.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNombre.Location = new System.Drawing.Point(712, 36);
            this.labelNombre.Name = "labelNombre";
            this.labelNombre.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.labelNombre.Size = new System.Drawing.Size(66, 30);
            this.labelNombre.TabIndex = 7;
            this.labelNombre.Text = "nombre";
            this.labelNombre.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.labelNombre.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // bunifuLabel2
            // 
            this.bunifuLabel2.AutoEllipsis = false;
            this.bunifuLabel2.CursorType = null;
            this.bunifuLabel2.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel2.Location = new System.Drawing.Point(638, 61);
            this.bunifuLabel2.Name = "bunifuLabel2";
            this.bunifuLabel2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel2.Size = new System.Drawing.Size(60, 30);
            this.bunifuLabel2.TabIndex = 6;
            this.bunifuLabel2.Text = "Correo:";
            this.bunifuLabel2.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel2.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // bunifuLabel1
            // 
            this.bunifuLabel1.AutoEllipsis = false;
            this.bunifuLabel1.CursorType = null;
            this.bunifuLabel1.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel1.Location = new System.Drawing.Point(628, 36);
            this.bunifuLabel1.Name = "bunifuLabel1";
            this.bunifuLabel1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel1.Size = new System.Drawing.Size(70, 30);
            this.bunifuLabel1.TabIndex = 5;
            this.bunifuLabel1.Text = "Nombre: ";
            this.bunifuLabel1.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel1.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // labelCorreo
            // 
            this.labelCorreo.AutoEllipsis = false;
            this.labelCorreo.CursorType = null;
            this.labelCorreo.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCorreo.Location = new System.Drawing.Point(712, 61);
            this.labelCorreo.Name = "labelCorreo";
            this.labelCorreo.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.labelCorreo.Size = new System.Drawing.Size(55, 30);
            this.labelCorreo.TabIndex = 0;
            this.labelCorreo.Text = "correo";
            this.labelCorreo.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.labelCorreo.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // dragPanel
            // 
            this.dragPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(138)))), ((int)(((byte)(7)))));
            this.dragPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.dragPanel.Location = new System.Drawing.Point(0, 0);
            this.dragPanel.Name = "dragPanel";
            this.dragPanel.Size = new System.Drawing.Size(995, 30);
            this.dragPanel.TabIndex = 2;
            // 
            // navBar
            // 
            this.navBar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(60)))), ((int)(((byte)(60)))));
            this.navBar.Controls.Add(this.agregarFormas);
            this.navBar.Controls.Add(this.agregarTour);
            this.navBar.Controls.Add(this.btnAgregarSede);
            this.navBar.Controls.Add(this.btnAgregarTrabajador);
            this.navBar.Controls.Add(this.btnCrearJornada);
            this.navBar.Dock = System.Windows.Forms.DockStyle.Left;
            this.navBar.Location = new System.Drawing.Point(0, 97);
            this.navBar.Name = "navBar";
            this.navBar.Size = new System.Drawing.Size(200, 535);
            this.navBar.TabIndex = 1;
            // 
            // agregarTour
            // 
            this.agregarTour.BackColor = System.Drawing.Color.Silver;
            this.agregarTour.Dock = System.Windows.Forms.DockStyle.Top;
            this.agregarTour.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.agregarTour.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.agregarTour.Location = new System.Drawing.Point(0, 204);
            this.agregarTour.Name = "agregarTour";
            this.agregarTour.Size = new System.Drawing.Size(200, 68);
            this.agregarTour.TabIndex = 3;
            this.agregarTour.Text = "Agregar Tours";
            this.agregarTour.UseVisualStyleBackColor = false;
            this.agregarTour.Click += new System.EventHandler(this.agregarTour_Click);
            // 
            // btnAgregarSede
            // 
            this.btnAgregarSede.BackColor = System.Drawing.Color.Silver;
            this.btnAgregarSede.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnAgregarSede.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAgregarSede.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAgregarSede.Location = new System.Drawing.Point(0, 136);
            this.btnAgregarSede.Name = "btnAgregarSede";
            this.btnAgregarSede.Size = new System.Drawing.Size(200, 68);
            this.btnAgregarSede.TabIndex = 2;
            this.btnAgregarSede.Text = "Agregar sede";
            this.btnAgregarSede.UseVisualStyleBackColor = false;
            this.btnAgregarSede.Click += new System.EventHandler(this.btnAgregarSede_Click);
            // 
            // btnAgregarTrabajador
            // 
            this.btnAgregarTrabajador.BackColor = System.Drawing.Color.Silver;
            this.btnAgregarTrabajador.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnAgregarTrabajador.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAgregarTrabajador.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAgregarTrabajador.Location = new System.Drawing.Point(0, 68);
            this.btnAgregarTrabajador.Name = "btnAgregarTrabajador";
            this.btnAgregarTrabajador.Size = new System.Drawing.Size(200, 68);
            this.btnAgregarTrabajador.TabIndex = 1;
            this.btnAgregarTrabajador.Text = "Agregar trabajadores";
            this.btnAgregarTrabajador.UseVisualStyleBackColor = false;
            this.btnAgregarTrabajador.Click += new System.EventHandler(this.btnAgregarTrabajador_Click);
            // 
            // btnCrearJornada
            // 
            this.btnCrearJornada.BackColor = System.Drawing.Color.Silver;
            this.btnCrearJornada.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnCrearJornada.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCrearJornada.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCrearJornada.Location = new System.Drawing.Point(0, 0);
            this.btnCrearJornada.Name = "btnCrearJornada";
            this.btnCrearJornada.Size = new System.Drawing.Size(200, 68);
            this.btnCrearJornada.TabIndex = 0;
            this.btnCrearJornada.Text = "Crear jornada";
            this.btnCrearJornada.UseVisualStyleBackColor = false;
            this.btnCrearJornada.Click += new System.EventHandler(this.btnCrearJornada_Click);
            // 
            // contentPanel
            // 
            this.contentPanel.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.contentPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.contentPanel.Location = new System.Drawing.Point(200, 97);
            this.contentPanel.Name = "contentPanel";
            this.contentPanel.Size = new System.Drawing.Size(795, 535);
            this.contentPanel.TabIndex = 2;
            // 
            // bunifuElipse1
            // 
            this.bunifuElipse1.ElipseRadius = 10;
            this.bunifuElipse1.TargetControl = this;
            // 
            // bunifuDragControl1
            // 
            this.bunifuDragControl1.Fixed = true;
            this.bunifuDragControl1.Horizontal = true;
            this.bunifuDragControl1.TargetControl = this.dragPanel;
            this.bunifuDragControl1.Vertical = true;
            // 
            // agregarFormas
            // 
            this.agregarFormas.BackColor = System.Drawing.Color.Silver;
            this.agregarFormas.Dock = System.Windows.Forms.DockStyle.Top;
            this.agregarFormas.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.agregarFormas.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.agregarFormas.Location = new System.Drawing.Point(0, 272);
            this.agregarFormas.Name = "agregarFormas";
            this.agregarFormas.Size = new System.Drawing.Size(200, 68);
            this.agregarFormas.TabIndex = 4;
            this.agregarFormas.Text = "Agregar formas de pago";
            this.agregarFormas.UseVisualStyleBackColor = false;
            this.agregarFormas.Click += new System.EventHandler(this.agregarFormas_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(995, 632);
            this.Controls.Add(this.contentPanel);
            this.Controls.Add(this.navBar);
            this.Controls.Add(this.mainBar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(995, 632);
            this.MinimumSize = new System.Drawing.Size(995, 632);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.mainBar.ResumeLayout(false);
            this.mainBar.PerformLayout();
            this.navBar.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel mainBar;
        private System.Windows.Forms.Panel dragPanel;
        private System.Windows.Forms.Panel navBar;
        private System.Windows.Forms.Panel contentPanel;
        private Bunifu.UI.WinForms.BunifuLabel labelCorreo;
        private Bunifu.Framework.UI.BunifuElipse bunifuElipse1;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton btnClose;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel1;
        private Bunifu.UI.WinForms.BunifuLabel labelNombre;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel2;
        private Bunifu.Framework.UI.BunifuDragControl bunifuDragControl1;
        private System.Windows.Forms.Button btnCrearJornada;
        private System.Windows.Forms.Button btnAgregarTrabajador;
        private System.Windows.Forms.Button btnAgregarSede;
        private System.Windows.Forms.Button agregarTour;
        private System.Windows.Forms.Button agregarFormas;
    }
}