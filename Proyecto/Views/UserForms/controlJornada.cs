﻿using MySql.Data.MySqlClient;
using Proyecto.Functions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyecto.Views.UserForms
{
    public partial class controlJornada : UserControl
    {
        private string cadena = string.Empty;

        public controlJornada()
        {
            InitializeComponent();
            selectorHoraInicio.Format = DateTimePickerFormat.Time;
            selectorHoraInicio.ShowUpDown = true;

            selectorHoraFin.Format = DateTimePickerFormat.Time;
            selectorHoraFin.ShowUpDown = true;

            DBUtils db = new DBUtils();
            cadena = db.returnConnectionString();
        }

        private void btnCrear_Click(object sender, EventArgs e)
        {
            if (txtNombre.Text == "") {
                MessageBox.Show("El nombre no puede quedar vacio", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (txtDias.Text == "") {
                MessageBox.Show("Los dias no pueden quedar vacios", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            try
            {
                using (MySqlConnection conexionBD = new MySqlConnection(cadena))
                {
                    conexionBD.Open();

                    using (MySqlCommand cmd = new MySqlCommand("insert into jornadas(nombre, dias, hora_inicio, hora_fin) values (@1, @2, @3, @4)", conexionBD))
                    {
                        //(Codigo,EstadoActual,Proveedor,Departamento,Color,TemporadaUso,Marca,PrecioCompra,PrecioVenta,Procedencia,TallasDisponibles,Descripcion,Stock)

                        //string formato = imagenA.RawFormat.ToString();
                        //MessageBox.Show(formato);
                        DateTime horaInicio = selectorHoraInicio.Value;
                        horaInicio.ToString("H:mm:ss");

                        DateTime horaFin = selectorHoraFin.Value;
                        horaFin.ToString("h:mm:ss");

                        cmd.Parameters.AddWithValue("@1", txtNombre.Text);
                        cmd.Parameters.AddWithValue("@2", Convert.ToInt32( txtDias.Text ));
                        cmd.Parameters.AddWithValue("@3", horaInicio);
                        cmd.Parameters.AddWithValue("@4", horaFin);

                        int estado = cmd.ExecuteNonQuery();
                        if (estado >= 1)
                        {
                            conexionBD.Close();

                            MessageBox.Show("Jornada creada satisfactoriamente", "Hecho", MessageBoxButtons.OK, MessageBoxIcon.Information);

                            txtNombre.Text = string.Empty;
                            txtDias.Text = string.Empty;

                            selectorHoraInicio.Value = DateTime.Now;
                            selectorHoraFin.Value = DateTime.Now;

                        }

                        conexionBD.Close();
                    }
                    conexionBD.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
    }
}
