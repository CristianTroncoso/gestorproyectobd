﻿using Proyecto.Functions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyecto.Views.UserForms
{
    public partial class controlTour : UserControl
    {
        public controlTour()
        {
            InitializeComponent();

            actualizarInformacion();

            timePicker.Format = DateTimePickerFormat.Time;
            timePicker.ShowUpDown = true;

            datePicker.Format = DateTimePickerFormat.Short;
        }
        private void actualizarInformacion()
        {
            Utils util = new Utils();
            util.cargarSedes(comboSede);
            util.cargarTour(comboTour);
            util.cargarTour(comboTour2);
            util.cargarTrabajadoresHabilitados(comboTrabajador);
        }

        private void txtPrecio_Leave(object sender, EventArgs e)
        {
            if (txtPrecio.Text != "0" && txtPrecio.Text != string.Empty)
            {
                Verifications verification = new Verifications();
                txtPrecio.Text = verification.formatearNumerosString(txtPrecio.Text);
            }
        }

        private void btnCrearComuna_Click(object sender, EventArgs e)
        {
            Utils util = new Utils();
            int idSede = util.getIdSede( comboSede.SelectedItem.ToString() );

            int precio = Convert.ToInt32(txtPrecio.Text.Replace(".", ""));
            util.crearTour(txtNombre.Text, txtDescripcion.Text, precio, idSede);

            txtNombre.ResetText();
            txtDescripcion.ResetText();
            txtPrecio.ResetText();

            actualizarInformacion();
        }

        private void btnCrearHorario_Click(object sender, EventArgs e)
        {
            Utils utils = new Utils();
            string horafecha = string.Empty;
            string hora = timePicker.Value.ToString(" H:mm:ss");
            string fecha = datePicker.Value.ToString("yyyy-MM-dd");
            horafecha = fecha + " " + hora;
            int cupos = Convert.ToInt32(txtCupos.Text);
            int idTour = utils.getIdTour( comboTour.SelectedItem.ToString() );
            
            utils.crearHorario(horafecha, cupos, idTour);
        }

        private void btnAgregarTrabajador_Click(object sender, EventArgs e)
        {
            Utils util = new Utils();
            int idTrabajador = util.getIdTrabajador( comboTrabajador.SelectedItem.ToString() );
            int idTour = util.getIdTour( comboTour2.SelectedItem.ToString() );
            if ( util.VerificarDuplicadoTrabajadoresTour(idTrabajador, idTour) ) {
                util.crearTrabajadorTour(idTrabajador, idTour);
            }
            else
            {
                MessageBox.Show("El trabajador ya esta asignado a este tour", "Trabajador duplicado", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

    }
}
