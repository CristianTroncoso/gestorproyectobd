﻿
namespace Proyecto.Views.UserForms
{
    partial class controlTour
    {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.bunifuLabel6 = new Bunifu.UI.WinForms.BunifuLabel();
            this.comboSede = new System.Windows.Forms.ComboBox();
            this.btnCrearComuna = new System.Windows.Forms.Button();
            this.bunifuLabel1 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel2 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel3 = new Bunifu.UI.WinForms.BunifuLabel();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.txtDescripcion = new System.Windows.Forms.TextBox();
            this.txtPrecio = new System.Windows.Forms.TextBox();
            this.bunifuLabel4 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel5 = new Bunifu.UI.WinForms.BunifuLabel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.bunifuLabel7 = new Bunifu.UI.WinForms.BunifuLabel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.bunifuLabel13 = new Bunifu.UI.WinForms.BunifuLabel();
            this.btnCrearHorario = new System.Windows.Forms.Button();
            this.bunifuLabel12 = new Bunifu.UI.WinForms.BunifuLabel();
            this.timePicker = new System.Windows.Forms.DateTimePicker();
            this.datePicker = new System.Windows.Forms.DateTimePicker();
            this.bunifuLabel11 = new Bunifu.UI.WinForms.BunifuLabel();
            this.txtCupos = new System.Windows.Forms.TextBox();
            this.bunifuLabel10 = new Bunifu.UI.WinForms.BunifuLabel();
            this.comboTour = new System.Windows.Forms.ComboBox();
            this.bunifuLabel9 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel8 = new Bunifu.UI.WinForms.BunifuLabel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnAgregarTrabajador = new System.Windows.Forms.Button();
            this.bunifuLabel16 = new Bunifu.UI.WinForms.BunifuLabel();
            this.comboTrabajador = new System.Windows.Forms.ComboBox();
            this.comboTour2 = new System.Windows.Forms.ComboBox();
            this.bunifuLabel15 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel14 = new Bunifu.UI.WinForms.BunifuLabel();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // bunifuLabel6
            // 
            this.bunifuLabel6.AutoEllipsis = false;
            this.bunifuLabel6.CursorType = null;
            this.bunifuLabel6.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel6.Location = new System.Drawing.Point(70, 64);
            this.bunifuLabel6.Name = "bunifuLabel6";
            this.bunifuLabel6.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel6.Size = new System.Drawing.Size(46, 30);
            this.bunifuLabel6.TabIndex = 30;
            this.bunifuLabel6.Text = "Sede:";
            this.bunifuLabel6.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel6.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // comboSede
            // 
            this.comboSede.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboSede.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboSede.FormattingEnabled = true;
            this.comboSede.Location = new System.Drawing.Point(122, 61);
            this.comboSede.Name = "comboSede";
            this.comboSede.Size = new System.Drawing.Size(250, 36);
            this.comboSede.TabIndex = 29;
            // 
            // btnCrearComuna
            // 
            this.btnCrearComuna.BackColor = System.Drawing.Color.LawnGreen;
            this.btnCrearComuna.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCrearComuna.Location = new System.Drawing.Point(122, 293);
            this.btnCrearComuna.Name = "btnCrearComuna";
            this.btnCrearComuna.Size = new System.Drawing.Size(250, 49);
            this.btnCrearComuna.TabIndex = 28;
            this.btnCrearComuna.Text = "Crear";
            this.btnCrearComuna.UseVisualStyleBackColor = false;
            this.btnCrearComuna.Click += new System.EventHandler(this.btnCrearComuna_Click);
            // 
            // bunifuLabel1
            // 
            this.bunifuLabel1.AutoEllipsis = false;
            this.bunifuLabel1.CursorType = null;
            this.bunifuLabel1.Font = new System.Drawing.Font("Poppins", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel1.Location = new System.Drawing.Point(362, 13);
            this.bunifuLabel1.Name = "bunifuLabel1";
            this.bunifuLabel1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel1.Size = new System.Drawing.Size(75, 60);
            this.bunifuLabel1.TabIndex = 27;
            this.bunifuLabel1.Text = "Tour";
            this.bunifuLabel1.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel1.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // bunifuLabel2
            // 
            this.bunifuLabel2.AutoEllipsis = false;
            this.bunifuLabel2.CursorType = null;
            this.bunifuLabel2.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel2.Location = new System.Drawing.Point(46, 103);
            this.bunifuLabel2.Name = "bunifuLabel2";
            this.bunifuLabel2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel2.Size = new System.Drawing.Size(70, 30);
            this.bunifuLabel2.TabIndex = 32;
            this.bunifuLabel2.Text = "Nombre:";
            this.bunifuLabel2.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel2.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // bunifuLabel3
            // 
            this.bunifuLabel3.AutoEllipsis = false;
            this.bunifuLabel3.CursorType = null;
            this.bunifuLabel3.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel3.Location = new System.Drawing.Point(16, 140);
            this.bunifuLabel3.Name = "bunifuLabel3";
            this.bunifuLabel3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel3.Size = new System.Drawing.Size(100, 30);
            this.bunifuLabel3.TabIndex = 33;
            this.bunifuLabel3.Text = "Descripcion:";
            this.bunifuLabel3.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel3.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // txtNombre
            // 
            this.txtNombre.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNombre.Location = new System.Drawing.Point(122, 103);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(250, 31);
            this.txtNombre.TabIndex = 34;
            // 
            // txtDescripcion
            // 
            this.txtDescripcion.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDescripcion.Location = new System.Drawing.Point(122, 140);
            this.txtDescripcion.MaxLength = 255;
            this.txtDescripcion.Multiline = true;
            this.txtDescripcion.Name = "txtDescripcion";
            this.txtDescripcion.Size = new System.Drawing.Size(250, 94);
            this.txtDescripcion.TabIndex = 35;
            // 
            // txtPrecio
            // 
            this.txtPrecio.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPrecio.Location = new System.Drawing.Point(141, 240);
            this.txtPrecio.Name = "txtPrecio";
            this.txtPrecio.Size = new System.Drawing.Size(231, 31);
            this.txtPrecio.TabIndex = 36;
            this.txtPrecio.Leave += new System.EventHandler(this.txtPrecio_Leave);
            // 
            // bunifuLabel4
            // 
            this.bunifuLabel4.AutoEllipsis = false;
            this.bunifuLabel4.CursorType = null;
            this.bunifuLabel4.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel4.Location = new System.Drawing.Point(61, 240);
            this.bunifuLabel4.Name = "bunifuLabel4";
            this.bunifuLabel4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel4.Size = new System.Drawing.Size(55, 30);
            this.bunifuLabel4.TabIndex = 37;
            this.bunifuLabel4.Text = "Precio:";
            this.bunifuLabel4.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel4.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // bunifuLabel5
            // 
            this.bunifuLabel5.AutoEllipsis = false;
            this.bunifuLabel5.CursorType = null;
            this.bunifuLabel5.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel5.Location = new System.Drawing.Point(122, 241);
            this.bunifuLabel5.Name = "bunifuLabel5";
            this.bunifuLabel5.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel5.Size = new System.Drawing.Size(13, 30);
            this.bunifuLabel5.TabIndex = 38;
            this.bunifuLabel5.Text = "$";
            this.bunifuLabel5.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel5.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.bunifuLabel7);
            this.panel1.Controls.Add(this.comboSede);
            this.panel1.Controls.Add(this.bunifuLabel5);
            this.panel1.Controls.Add(this.btnCrearComuna);
            this.panel1.Controls.Add(this.bunifuLabel4);
            this.panel1.Controls.Add(this.bunifuLabel6);
            this.panel1.Controls.Add(this.txtPrecio);
            this.panel1.Controls.Add(this.bunifuLabel2);
            this.panel1.Controls.Add(this.txtDescripcion);
            this.panel1.Controls.Add(this.bunifuLabel3);
            this.panel1.Controls.Add(this.txtNombre);
            this.panel1.Location = new System.Drawing.Point(8, 62);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(418, 456);
            this.panel1.TabIndex = 39;
            // 
            // bunifuLabel7
            // 
            this.bunifuLabel7.AutoEllipsis = false;
            this.bunifuLabel7.CursorType = null;
            this.bunifuLabel7.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel7.Location = new System.Drawing.Point(171, 3);
            this.bunifuLabel7.Name = "bunifuLabel7";
            this.bunifuLabel7.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel7.Size = new System.Drawing.Size(84, 30);
            this.bunifuLabel7.TabIndex = 39;
            this.bunifuLabel7.Text = "Crear tour";
            this.bunifuLabel7.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel7.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.bunifuLabel13);
            this.panel2.Controls.Add(this.btnCrearHorario);
            this.panel2.Controls.Add(this.bunifuLabel12);
            this.panel2.Controls.Add(this.timePicker);
            this.panel2.Controls.Add(this.datePicker);
            this.panel2.Controls.Add(this.bunifuLabel11);
            this.panel2.Controls.Add(this.txtCupos);
            this.panel2.Controls.Add(this.bunifuLabel10);
            this.panel2.Controls.Add(this.comboTour);
            this.panel2.Controls.Add(this.bunifuLabel9);
            this.panel2.Controls.Add(this.bunifuLabel8);
            this.panel2.Location = new System.Drawing.Point(432, 62);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(351, 249);
            this.panel2.TabIndex = 40;
            // 
            // bunifuLabel13
            // 
            this.bunifuLabel13.AutoEllipsis = false;
            this.bunifuLabel13.CursorType = null;
            this.bunifuLabel13.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel13.Location = new System.Drawing.Point(248, 83);
            this.bunifuLabel13.Name = "bunifuLabel13";
            this.bunifuLabel13.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel13.Size = new System.Drawing.Size(77, 30);
            this.bunifuLabel13.TabIndex = 48;
            this.bunifuLabel13.Text = "personas";
            this.bunifuLabel13.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel13.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // btnCrearHorario
            // 
            this.btnCrearHorario.BackColor = System.Drawing.Color.LawnGreen;
            this.btnCrearHorario.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCrearHorario.Location = new System.Drawing.Point(75, 190);
            this.btnCrearHorario.Name = "btnCrearHorario";
            this.btnCrearHorario.Size = new System.Drawing.Size(250, 49);
            this.btnCrearHorario.TabIndex = 40;
            this.btnCrearHorario.Text = "Crear";
            this.btnCrearHorario.UseVisualStyleBackColor = false;
            this.btnCrearHorario.Click += new System.EventHandler(this.btnCrearHorario_Click);
            // 
            // bunifuLabel12
            // 
            this.bunifuLabel12.AutoEllipsis = false;
            this.bunifuLabel12.CursorType = null;
            this.bunifuLabel12.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel12.Location = new System.Drawing.Point(75, 154);
            this.bunifuLabel12.Name = "bunifuLabel12";
            this.bunifuLabel12.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel12.Size = new System.Drawing.Size(44, 30);
            this.bunifuLabel12.TabIndex = 47;
            this.bunifuLabel12.Text = "Hora:";
            this.bunifuLabel12.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel12.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // timePicker
            // 
            this.timePicker.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timePicker.Location = new System.Drawing.Point(122, 154);
            this.timePicker.Name = "timePicker";
            this.timePicker.Size = new System.Drawing.Size(204, 31);
            this.timePicker.TabIndex = 46;
            // 
            // datePicker
            // 
            this.datePicker.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.datePicker.Location = new System.Drawing.Point(122, 117);
            this.datePicker.Name = "datePicker";
            this.datePicker.Size = new System.Drawing.Size(204, 31);
            this.datePicker.TabIndex = 45;
            // 
            // bunifuLabel11
            // 
            this.bunifuLabel11.AutoEllipsis = false;
            this.bunifuLabel11.CursorType = null;
            this.bunifuLabel11.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel11.Location = new System.Drawing.Point(61, 117);
            this.bunifuLabel11.Name = "bunifuLabel11";
            this.bunifuLabel11.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel11.Size = new System.Drawing.Size(55, 30);
            this.bunifuLabel11.TabIndex = 44;
            this.bunifuLabel11.Text = "Fecha:";
            this.bunifuLabel11.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel11.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // txtCupos
            // 
            this.txtCupos.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCupos.Location = new System.Drawing.Point(122, 82);
            this.txtCupos.Name = "txtCupos";
            this.txtCupos.Size = new System.Drawing.Size(123, 31);
            this.txtCupos.TabIndex = 40;
            // 
            // bunifuLabel10
            // 
            this.bunifuLabel10.AutoEllipsis = false;
            this.bunifuLabel10.CursorType = null;
            this.bunifuLabel10.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel10.Location = new System.Drawing.Point(62, 82);
            this.bunifuLabel10.Name = "bunifuLabel10";
            this.bunifuLabel10.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel10.Size = new System.Drawing.Size(57, 30);
            this.bunifuLabel10.TabIndex = 43;
            this.bunifuLabel10.Text = "Cupos:";
            this.bunifuLabel10.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel10.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // comboTour
            // 
            this.comboTour.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboTour.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboTour.FormattingEnabled = true;
            this.comboTour.Location = new System.Drawing.Point(122, 39);
            this.comboTour.Name = "comboTour";
            this.comboTour.Size = new System.Drawing.Size(204, 36);
            this.comboTour.TabIndex = 41;
            // 
            // bunifuLabel9
            // 
            this.bunifuLabel9.AutoEllipsis = false;
            this.bunifuLabel9.CursorType = null;
            this.bunifuLabel9.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel9.Location = new System.Drawing.Point(78, 42);
            this.bunifuLabel9.Name = "bunifuLabel9";
            this.bunifuLabel9.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel9.Size = new System.Drawing.Size(41, 30);
            this.bunifuLabel9.TabIndex = 42;
            this.bunifuLabel9.Text = "Tour:";
            this.bunifuLabel9.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel9.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // bunifuLabel8
            // 
            this.bunifuLabel8.AutoEllipsis = false;
            this.bunifuLabel8.CursorType = null;
            this.bunifuLabel8.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel8.Location = new System.Drawing.Point(128, 3);
            this.bunifuLabel8.Name = "bunifuLabel8";
            this.bunifuLabel8.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel8.Size = new System.Drawing.Size(109, 30);
            this.bunifuLabel8.TabIndex = 40;
            this.bunifuLabel8.Text = "Crear horario";
            this.bunifuLabel8.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel8.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.btnAgregarTrabajador);
            this.panel3.Controls.Add(this.bunifuLabel16);
            this.panel3.Controls.Add(this.comboTrabajador);
            this.panel3.Controls.Add(this.comboTour2);
            this.panel3.Controls.Add(this.bunifuLabel15);
            this.panel3.Controls.Add(this.bunifuLabel14);
            this.panel3.Location = new System.Drawing.Point(432, 317);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(360, 201);
            this.panel3.TabIndex = 41;
            // 
            // btnAgregarTrabajador
            // 
            this.btnAgregarTrabajador.BackColor = System.Drawing.Color.LawnGreen;
            this.btnAgregarTrabajador.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAgregarTrabajador.Location = new System.Drawing.Point(75, 128);
            this.btnAgregarTrabajador.Name = "btnAgregarTrabajador";
            this.btnAgregarTrabajador.Size = new System.Drawing.Size(250, 49);
            this.btnAgregarTrabajador.TabIndex = 49;
            this.btnAgregarTrabajador.Text = "Añadir";
            this.btnAgregarTrabajador.UseVisualStyleBackColor = false;
            this.btnAgregarTrabajador.Click += new System.EventHandler(this.btnAgregarTrabajador_Click);
            // 
            // bunifuLabel16
            // 
            this.bunifuLabel16.AutoEllipsis = false;
            this.bunifuLabel16.CursorType = null;
            this.bunifuLabel16.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel16.Location = new System.Drawing.Point(23, 80);
            this.bunifuLabel16.Name = "bunifuLabel16";
            this.bunifuLabel16.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel16.Size = new System.Drawing.Size(96, 30);
            this.bunifuLabel16.TabIndex = 52;
            this.bunifuLabel16.Text = "Trabajador:";
            this.bunifuLabel16.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel16.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // comboTrabajador
            // 
            this.comboTrabajador.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboTrabajador.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboTrabajador.FormattingEnabled = true;
            this.comboTrabajador.Location = new System.Drawing.Point(122, 77);
            this.comboTrabajador.Name = "comboTrabajador";
            this.comboTrabajador.Size = new System.Drawing.Size(204, 36);
            this.comboTrabajador.TabIndex = 51;
            // 
            // comboTour2
            // 
            this.comboTour2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboTour2.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboTour2.FormattingEnabled = true;
            this.comboTour2.Location = new System.Drawing.Point(122, 35);
            this.comboTour2.Name = "comboTour2";
            this.comboTour2.Size = new System.Drawing.Size(204, 36);
            this.comboTour2.TabIndex = 49;
            // 
            // bunifuLabel15
            // 
            this.bunifuLabel15.AutoEllipsis = false;
            this.bunifuLabel15.CursorType = null;
            this.bunifuLabel15.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel15.Location = new System.Drawing.Point(78, 38);
            this.bunifuLabel15.Name = "bunifuLabel15";
            this.bunifuLabel15.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel15.Size = new System.Drawing.Size(41, 30);
            this.bunifuLabel15.TabIndex = 50;
            this.bunifuLabel15.Text = "Tour:";
            this.bunifuLabel15.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel15.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // bunifuLabel14
            // 
            this.bunifuLabel14.AutoEllipsis = false;
            this.bunifuLabel14.CursorType = null;
            this.bunifuLabel14.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel14.Location = new System.Drawing.Point(109, 3);
            this.bunifuLabel14.Name = "bunifuLabel14";
            this.bunifuLabel14.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel14.Size = new System.Drawing.Size(147, 30);
            this.bunifuLabel14.TabIndex = 49;
            this.bunifuLabel14.Text = "Añadir trabajador";
            this.bunifuLabel14.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel14.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // controlTour
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.bunifuLabel1);
            this.Name = "controlTour";
            this.Size = new System.Drawing.Size(795, 535);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel6;
        private System.Windows.Forms.ComboBox comboSede;
        private System.Windows.Forms.Button btnCrearComuna;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel1;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel2;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel3;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.TextBox txtDescripcion;
        private System.Windows.Forms.TextBox txtPrecio;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel4;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel5;
        private System.Windows.Forms.Panel panel1;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel7;
        private System.Windows.Forms.Panel panel2;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel8;
        private System.Windows.Forms.ComboBox comboTour;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel9;
        private System.Windows.Forms.TextBox txtCupos;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel10;
        private System.Windows.Forms.DateTimePicker datePicker;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel11;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel12;
        private System.Windows.Forms.DateTimePicker timePicker;
        private System.Windows.Forms.Button btnCrearHorario;
        private System.Windows.Forms.Panel panel3;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel13;
        private System.Windows.Forms.Button btnAgregarTrabajador;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel16;
        private System.Windows.Forms.ComboBox comboTrabajador;
        private System.Windows.Forms.ComboBox comboTour2;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel15;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel14;
    }
}
