﻿using MySql.Data.MySqlClient;
using Proyecto.Functions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyecto.Views.UserForms
{
    public partial class controlTrabajadores : UserControl
    {
        private string cadena = string.Empty;
        public controlTrabajadores()
        {
            InitializeComponent();

            DBUtils db = new DBUtils();
            cadena = db.returnConnectionString();

            comboAcceso.SelectedIndex = 0;

            Utils util = new Utils();
            util.cargarTrabajadores(comboRut);
            util.cargarTrabajadoresHabilitados(comboRut2);

            if (comboRut.Items.Count >= 1) {
                comboRut.SelectedIndex = 0;
            }
            if (comboRut2.Items.Count >= 1)
            {
                comboRut2.SelectedIndex = 0;
            }
            util.cargarJornadas(comboJornada);
            if (comboJornada.Items.Count >= 1) {
                comboJornada.SelectedIndex = 0;
            }
            util.cargarSedes(comboSede);
            if (comboSede.Items.Count >= 1) {
                comboSede.SelectedIndex = 0;
            }
        }
        
        private void btnAsignar_Click(object sender, EventArgs e)
        {
            Utils util = new Utils();
            try
            {
                using (MySqlConnection conexionBD = new MySqlConnection(cadena))
                {
                    conexionBD.Open();

                    using (MySqlCommand cmd = new MySqlCommand("insert into trabajadores(admin, id_persona, id_jornada) values (@1, @2, @3)", conexionBD))
                    {
                        int esAdmin;
                        if ( comboAcceso.SelectedIndex == 0) {
                            esAdmin = 0;
                        }
                        else
                        {
                            esAdmin = 1;
                        }

                        cmd.Parameters.AddWithValue("@1", esAdmin);
                        cmd.Parameters.AddWithValue("@2", util.getIdPersona(comboRut.SelectedItem.ToString()));
                        cmd.Parameters.AddWithValue("@3", util.getIdJornada(comboJornada.SelectedItem.ToString()));

                        int estado = cmd.ExecuteNonQuery();
                        if (estado >= 1)
                        {
                            conexionBD.Close();
                            MessageBox.Show("Trabajador agregado correctamente", "Hecho", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }

                        conexionBD.Close();
                    }
                    conexionBD.Close();
                }
            }
            catch (Exception ex)
            {
                if (ex.ToString().Contains("Duplicate entry")) {
                    MessageBox.Show("El trabajador ya existe", "Trabajador duplicado", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }

            

        }

        private void btnAsignarSede_Click(object sender, EventArgs e)
        {
            //  WIP
            Utils util = new Utils();
            if (util.VerificarDuplicadoTrabajadoresSedes(util.getIdTrabajador(comboRut2.SelectedItem.ToString()), util.getIdSede(comboSede.SelectedItem.ToString()))) {
                util.asignarSede(util.getIdTrabajador(comboRut2.SelectedItem.ToString()), util.getIdSede(comboSede.SelectedItem.ToString()));
            }
            else
            {
                MessageBox.Show("El trabajador ya esta asignado a esta sede", "Trabajador duplicado", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
