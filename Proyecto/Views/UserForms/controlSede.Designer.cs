﻿
namespace Proyecto.Views.UserForms
{
    partial class controlSede
    {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.bunifuLabel1 = new Bunifu.UI.WinForms.BunifuLabel();
            this.btnCrear = new System.Windows.Forms.Button();
            this.comboRegion = new System.Windows.Forms.ComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.btnCrearRegion = new System.Windows.Forms.Button();
            this.bunifuLabel3 = new Bunifu.UI.WinForms.BunifuLabel();
            this.comboCiudad = new System.Windows.Forms.ComboBox();
            this.comboComunas = new System.Windows.Forms.ComboBox();
            this.bunifuLabel4 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel2 = new Bunifu.UI.WinForms.BunifuLabel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.comboRegionVinculada = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.bunifuLabel5 = new Bunifu.UI.WinForms.BunifuLabel();
            this.btnCrearCiudad = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnActualizar = new System.Windows.Forms.Button();
            this.bunifuLabel6 = new Bunifu.UI.WinForms.BunifuLabel();
            this.comboCiudadVinculada = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnCrearComuna = new System.Windows.Forms.Button();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.bunifuLabel7 = new Bunifu.UI.WinForms.BunifuLabel();
            this.comboComunaVinculada = new System.Windows.Forms.ComboBox();
            this.bunifuLabel8 = new Bunifu.UI.WinForms.BunifuLabel();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // bunifuLabel1
            // 
            this.bunifuLabel1.AutoEllipsis = false;
            this.bunifuLabel1.CursorType = null;
            this.bunifuLabel1.Font = new System.Drawing.Font("Poppins", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel1.Location = new System.Drawing.Point(305, 15);
            this.bunifuLabel1.Name = "bunifuLabel1";
            this.bunifuLabel1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel1.Size = new System.Drawing.Size(184, 60);
            this.bunifuLabel1.TabIndex = 11;
            this.bunifuLabel1.Text = "Crear Sede";
            this.bunifuLabel1.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel1.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // btnCrear
            // 
            this.btnCrear.BackColor = System.Drawing.Color.PaleGreen;
            this.btnCrear.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCrear.Location = new System.Drawing.Point(475, 420);
            this.btnCrear.Name = "btnCrear";
            this.btnCrear.Size = new System.Drawing.Size(186, 60);
            this.btnCrear.TabIndex = 10;
            this.btnCrear.Text = "Crear sede";
            this.btnCrear.UseVisualStyleBackColor = false;
            this.btnCrear.Click += new System.EventHandler(this.btnCrear_Click);
            // 
            // comboRegion
            // 
            this.comboRegion.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboRegion.FormattingEnabled = true;
            this.comboRegion.Items.AddRange(new object[] {
            "Arica y Parinacota",
            "Tarapaca",
            "Antofagasta",
            "Atacama",
            "Coquimbo",
            "Valparaiso",
            "Metropolitana",
            "Libertador General Bernardo O\'higgins",
            "Maule",
            "Nuble",
            "Biobio",
            "Araucania",
            "Los Rios",
            "Los Lagos",
            "General Carlos Ibanez del Campo",
            "Magallanes y de la antartica Chilena"});
            this.comboRegion.Location = new System.Drawing.Point(95, 52);
            this.comboRegion.Name = "comboRegion";
            this.comboRegion.Size = new System.Drawing.Size(215, 36);
            this.comboRegion.TabIndex = 14;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.btnCrearRegion);
            this.panel1.Controls.Add(this.bunifuLabel3);
            this.panel1.Controls.Add(this.comboRegion);
            this.panel1.Location = new System.Drawing.Point(16, 96);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(332, 187);
            this.panel1.TabIndex = 15;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 2);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(27, 30);
            this.label1.TabIndex = 22;
            this.label1.Text = " 1 ";
            // 
            // btnCrearRegion
            // 
            this.btnCrearRegion.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCrearRegion.Location = new System.Drawing.Point(95, 111);
            this.btnCrearRegion.Name = "btnCrearRegion";
            this.btnCrearRegion.Size = new System.Drawing.Size(186, 49);
            this.btnCrearRegion.TabIndex = 16;
            this.btnCrearRegion.Text = "Crear region";
            this.btnCrearRegion.UseVisualStyleBackColor = true;
            this.btnCrearRegion.Click += new System.EventHandler(this.btnCrearRegion_Click);
            // 
            // bunifuLabel3
            // 
            this.bunifuLabel3.AutoEllipsis = false;
            this.bunifuLabel3.CursorType = null;
            this.bunifuLabel3.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel3.Location = new System.Drawing.Point(26, 58);
            this.bunifuLabel3.Name = "bunifuLabel3";
            this.bunifuLabel3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel3.Size = new System.Drawing.Size(58, 30);
            this.bunifuLabel3.TabIndex = 15;
            this.bunifuLabel3.Text = "Region";
            this.bunifuLabel3.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel3.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // comboCiudad
            // 
            this.comboCiudad.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboCiudad.FormattingEnabled = true;
            this.comboCiudad.Items.AddRange(new object[] {
            "Antofagasta\t",
            "Arica\t",
            "Buin\t",
            "Calama\t",
            "Chillán-Chillan Viejo\t",
            "Copiapó\t",
            "Coyhaique\t",
            "Curicó\t",
            "Gran Concepción\t",
            "Gran Iquique\t",
            "Gran La Serena\t",
            "Gran Puerto Montt\t",
            "Gran Quillota\t",
            "Gran Rancagua\t",
            "Gran Santiago\t",
            "Gran Temuco\t",
            "Gran Valparaíso\t",
            "Limache-Olmué\t",
            "Linares\t",
            "Los Andes-Calle Larga-San Esteban\t",
            "Los Ángeles\t",
            "Melipilla\t",
            "Osorno\t",
            "Ovalle\t",
            "Punta Arenas\t",
            "San Antonio\t",
            "San Felipe\t",
            "San Fernando\t",
            "Talagante\tMetropolitana",
            "Talca\t",
            "Valdivia\t"});
            this.comboCiudad.Location = new System.Drawing.Point(95, 101);
            this.comboCiudad.Name = "comboCiudad";
            this.comboCiudad.Size = new System.Drawing.Size(215, 36);
            this.comboCiudad.TabIndex = 16;
            // 
            // comboComunas
            // 
            this.comboComunas.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboComunas.FormattingEnabled = true;
            this.comboComunas.Items.AddRange(new object[] {
            "ALGARROBO",
            "ALHUE",
            "ALTO BIOBIO",
            "ALTO DEL CARMEN",
            "ALTO HOSPICIO",
            "ANCUD",
            "ANDACOLLO",
            "ANGOL",
            "ANTOFAGASTA",
            "ANTUCO",
            "ARAUCO",
            "ARICA",
            "AYSEN",
            "BUIN",
            "BULNES",
            "CABILDO",
            "CABO DE HORNOS",
            "CABRERO",
            "CALAMA",
            "CALBUCO",
            "CALDERA",
            "CALERA DE TANGO",
            "CALLE LARGA",
            "CAMARONES",
            "CAMINA",
            "CANELA",
            "CANETE",
            "CARAHUE",
            "CARTAGENA",
            "CASABLANCA",
            "CASTRO",
            "CATEMU",
            "CAUQUENES",
            "CERRILLOS",
            "CERRO NAVIA",
            "CHAITEN",
            "CHANARAL",
            "CHANCO",
            "CHEPICA",
            "CHIGUAYANTE",
            "CHILE CHICO",
            "CHILLAN",
            "CHILLAN VIEJO",
            "CHIMBARONGO",
            "CHOLCHOL",
            "CHONCHI",
            "CISNES",
            "COBQUECURA",
            "COCHAMO",
            "COCHRANE",
            "CODEGUA",
            "COELEMU",
            "COIHUECO",
            "COINCO",
            "COLBUN",
            "COLCHANE",
            "COLINA",
            "COLLIPULLI",
            "COLTAUCO",
            "COMBARBALA",
            "CONCEPCION",
            "CONCHALI",
            "CONCON",
            "CONSTITUCION",
            "CONTULMO",
            "COPIAPO",
            "COQUIMBO",
            "CORONEL",
            "CORRAL",
            "COYHAIQUE",
            "CUNCO",
            "CURACAUTIN",
            "CURACAVI",
            "CURACO DE VELEZ",
            "CURANILAHUE",
            "CURARREHUE",
            "CUREPTO",
            "CURICO",
            "DALCAHUE",
            "DIEGO DE ALMAGRO",
            "DONIHUE",
            "EL BOSQUE",
            "EL CARMEN",
            "EL MONTE",
            "EL QUISCO",
            "EL TABO",
            "EMPEDRADO",
            "ERCILLA",
            "ESTACION CENTRAL",
            "FLORIDA",
            "FREIRE",
            "FREIRINA",
            "FRESIA",
            "FRUTILLAR",
            "FUTALEUFU",
            "FUTRONO",
            "GALVARINO",
            "GENERAL LAGOS",
            "GORBEA",
            "GRANEROS",
            "GUAITECAS",
            "HIJUELAS",
            "HUALAIHUE",
            "HUALANE",
            "HUALPEN",
            "HUALQUI",
            "HUARA",
            "HUASCO",
            "HUECHURABA",
            "ILLAPEL",
            "INDEPENDENCIA",
            "IQUIQUE",
            "ISLA DE MAIPO",
            "ISLA DE PASCUA",
            "JUAN FERNANDEZ",
            "LA CALERA",
            "LA CISTERNA",
            "LA CRUZ",
            "LA ESTRELLA",
            "LA FLORIDA",
            "LA GRANJA",
            "LA HIGUERA",
            "LA LIGUA",
            "LA PINTANA",
            "LA REINA",
            "LA SERENA",
            "LA UNION",
            "LAGO RANCO",
            "LAGO VERDE",
            "LAGUNA BLANCA",
            "LAJA",
            "LAMPA",
            "LANCO",
            "LAS CABRAS",
            "LAS CONDES",
            "LAUTARO",
            "LEBU",
            "LICANTEN",
            "LIMACHE",
            "LINARES",
            "LITUECHE",
            "LLANQUIHUE",
            "LLAY-LLAY",
            "LO BARNECHEA",
            "LO ESPEJO",
            "LO PRADO",
            "LOLOL",
            "LONCOCHE",
            "LONGAVI",
            "LONQUIMAY",
            "LOS ALAMOS",
            "LOS ANDES",
            "LOS ANGELES",
            "LOS LAGOS",
            "LOS MUERMOS",
            "LOS SAUCES",
            "LOS VILOS",
            "LOTA",
            "LUMACO",
            "MACHALI",
            "MACUL",
            "MAFIL",
            "MAIPU",
            "MALLOA",
            "MARCHIGUE",
            "MARIA ELENA",
            "MARIA PINTO",
            "MARIQUINA",
            "MAULE",
            "MAULLIN",
            "MEJILLONES",
            "MELIPEUCO",
            "MELIPILLA",
            "MOLINA",
            "MONTE PATRIA",
            "MULCHEN",
            "NACIMIENTO",
            "NANCAGUA",
            "NATALES",
            "NAVIDAD",
            "NEGRETE",
            "NINHUE",
            "NIQUEN",
            "NOGALES",
            "NUEVA IMPERIAL",
            "NUNOA",
            "OHIGGINS",
            "OLIVAR",
            "OLLAGUE",
            "OLMUE",
            "OSORNO",
            "OVALLE",
            "PADRE HURTADO",
            "PADRE LAS CASAS",
            "PAIHUANO",
            "PAILLACO",
            "PAINE",
            "PALENA",
            "PALMILLA",
            "PANGUIPULLI",
            "PANQUEHUE",
            "PAPUDO",
            "PAREDONES",
            "PARRAL",
            "PEDRO AGUIRRE CERDA",
            "PELARCO",
            "PELLUHUE",
            "PEMUCO",
            "PENAFLOR",
            "PENALOLEN",
            "PENCAHUE",
            "PENCO",
            "PERALILLO",
            "PERQUENCO",
            "PETORCA",
            "PEUMO",
            "PICA",
            "PICHIDEGUA",
            "PICHILEMU",
            "PINTO",
            "PIRQUE",
            "PITRUFQUEN",
            "PLACILLA",
            "PORTEZUELO",
            "PORVENIR",
            "POZO ALMONTE",
            "PRIMAVERA",
            "PROVIDENCIA",
            "PUCHUNCAVI",
            "PUCON",
            "PUDAHUEL",
            "PUENTE ALTO",
            "PUERTO MONTT",
            "PUERTO OCTAY",
            "PUERTO VARAS",
            "PUMANQUE",
            "PUNITAQUI",
            "PUNTA ARENAS",
            "PUQUELDON",
            "PUREN",
            "PURRANQUE",
            "PUTAENDO",
            "PUTRE",
            "PUYEHUE",
            "QUEILEN",
            "QUELLON",
            "QUEMCHI",
            "QUILACO",
            "QUILICURA",
            "QUILLECO",
            "QUILLON",
            "QUILLOTA",
            "QUILPUE",
            "QUINCHAO",
            "QUINTA DE TILCOCO",
            "QUINTA NORMAL",
            "QUINTERO",
            "QUIRIHUE",
            "RANCAGUA",
            "RANQUIL",
            "RAUCO",
            "RECOLETA",
            "RENAICO",
            "RENCA",
            "RENGO",
            "REQUINOA",
            "RETIRO",
            "RINCONADA",
            "RIO BUENO",
            "RIO CLARO",
            "RIO HURTADO",
            "RIO IBANEZ",
            "RIO NEGRO",
            "RIO VERDE",
            "ROMERAL",
            "SAAVEDRA",
            "SAGRADA FAMILIA",
            "SALAMANCA",
            "SAN ANTONIO",
            "SAN BERNARDO",
            "SAN CARLOS",
            "SAN CLEMENTE",
            "SAN ESTEBAN",
            "SAN FABIAN",
            "SAN FELIPE",
            "SAN FERNANDO",
            "SAN FRANCISCO DE MOSTAZAL",
            "SAN GREGORIO",
            "SAN IGNACIO",
            "SAN JAVIER",
            "SAN JOAQUIN",
            "SAN JOSE DE MAIPO",
            "SAN JUAN DE LA COSTA",
            "SAN MIGUEL",
            "SAN NICOLAS",
            "SAN PABLO",
            "SAN PEDRO",
            "SAN PEDRO DE ATACAMA",
            "SAN PEDRO DE LA PAZ",
            "SAN RAFAEL",
            "SAN RAMON",
            "SAN ROSENDO",
            "SAN VICENTE",
            "SANTA BARBARA",
            "SANTA CRUZ",
            "SANTA JUANA",
            "SANTA MARIA",
            "SANTIAGO",
            "SANTIAGO OESTE",
            "SANTIAGO SUR",
            "SANTO DOMINGO",
            "SIERRA GORDA",
            "TALAGANTE",
            "TALCA",
            "TALCAHUANO",
            "TALTAL",
            "TEMUCO",
            "TENO",
            "TEODORO SCHMIDT",
            "TIERRA AMARILLA",
            "TIL-TIL",
            "TIMAUKEL",
            "TIRUA",
            "TOCOPILLA",
            "TOLTEN",
            "TOME",
            "TORRES DEL PAINE",
            "TORTEL",
            "TRAIGUEN",
            "TREHUACO",
            "TUCAPEL",
            "VALDIVIA",
            "VALLENAR",
            "VALPARAISO",
            "VICHUQUEN",
            "VICTORIA",
            "VICUNA",
            "VILCUN",
            "VILLA ALEGRE",
            "VILLA ALEMANA",
            "VILLARRICA",
            "VINA DEL MAR",
            "VITACURA",
            "YERBAS BUENAS",
            "YUMBEL",
            "YUNGAY",
            "ZAPALLAR"});
            this.comboComunas.Location = new System.Drawing.Point(121, 88);
            this.comboComunas.Name = "comboComunas";
            this.comboComunas.Size = new System.Drawing.Size(215, 36);
            this.comboComunas.TabIndex = 17;
            // 
            // bunifuLabel4
            // 
            this.bunifuLabel4.AutoEllipsis = false;
            this.bunifuLabel4.CursorType = null;
            this.bunifuLabel4.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel4.Location = new System.Drawing.Point(19, 101);
            this.bunifuLabel4.Name = "bunifuLabel4";
            this.bunifuLabel4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel4.Size = new System.Drawing.Size(65, 30);
            this.bunifuLabel4.TabIndex = 18;
            this.bunifuLabel4.Text = "Ciudad:";
            this.bunifuLabel4.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel4.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // bunifuLabel2
            // 
            this.bunifuLabel2.AutoEllipsis = false;
            this.bunifuLabel2.CursorType = null;
            this.bunifuLabel2.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel2.Location = new System.Drawing.Point(35, 91);
            this.bunifuLabel2.Name = "bunifuLabel2";
            this.bunifuLabel2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel2.Size = new System.Drawing.Size(75, 30);
            this.bunifuLabel2.TabIndex = 19;
            this.bunifuLabel2.Text = "Comuna:";
            this.bunifuLabel2.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel2.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.comboRegionVinculada);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.bunifuLabel5);
            this.panel2.Controls.Add(this.btnCrearCiudad);
            this.panel2.Controls.Add(this.comboCiudad);
            this.panel2.Controls.Add(this.bunifuLabel4);
            this.panel2.Location = new System.Drawing.Point(16, 293);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(332, 227);
            this.panel2.TabIndex = 20;
            // 
            // comboRegionVinculada
            // 
            this.comboRegionVinculada.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboRegionVinculada.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboRegionVinculada.FormattingEnabled = true;
            this.comboRegionVinculada.Location = new System.Drawing.Point(95, 59);
            this.comboRegionVinculada.Name = "comboRegionVinculada";
            this.comboRegionVinculada.Size = new System.Drawing.Size(215, 36);
            this.comboRegionVinculada.TabIndex = 24;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label2.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 2);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 30);
            this.label2.TabIndex = 23;
            this.label2.Text = " 2 ";
            // 
            // bunifuLabel5
            // 
            this.bunifuLabel5.AutoEllipsis = false;
            this.bunifuLabel5.CursorType = null;
            this.bunifuLabel5.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel5.Location = new System.Drawing.Point(26, 65);
            this.bunifuLabel5.Name = "bunifuLabel5";
            this.bunifuLabel5.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel5.Size = new System.Drawing.Size(58, 30);
            this.bunifuLabel5.TabIndex = 17;
            this.bunifuLabel5.Text = "Region";
            this.bunifuLabel5.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel5.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // btnCrearCiudad
            // 
            this.btnCrearCiudad.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCrearCiudad.Location = new System.Drawing.Point(95, 157);
            this.btnCrearCiudad.Name = "btnCrearCiudad";
            this.btnCrearCiudad.Size = new System.Drawing.Size(186, 49);
            this.btnCrearCiudad.TabIndex = 17;
            this.btnCrearCiudad.Text = "Crear ciudad";
            this.btnCrearCiudad.UseVisualStyleBackColor = true;
            this.btnCrearCiudad.Click += new System.EventHandler(this.btnCrearCiudad_Click);
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.bunifuLabel6);
            this.panel3.Controls.Add(this.comboCiudadVinculada);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.btnCrearComuna);
            this.panel3.Controls.Add(this.comboComunas);
            this.panel3.Controls.Add(this.bunifuLabel2);
            this.panel3.Location = new System.Drawing.Point(354, 96);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(413, 187);
            this.panel3.TabIndex = 21;
            // 
            // btnActualizar
            // 
            this.btnActualizar.Image = global::Proyecto.Properties.Resources.actualizar;
            this.btnActualizar.Location = new System.Drawing.Point(733, 15);
            this.btnActualizar.Name = "btnActualizar";
            this.btnActualizar.Size = new System.Drawing.Size(46, 44);
            this.btnActualizar.TabIndex = 28;
            this.btnActualizar.UseVisualStyleBackColor = true;
            this.btnActualizar.Click += new System.EventHandler(this.btnActualizar_Click);
            // 
            // bunifuLabel6
            // 
            this.bunifuLabel6.AutoEllipsis = false;
            this.bunifuLabel6.CursorType = null;
            this.bunifuLabel6.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel6.Location = new System.Drawing.Point(45, 52);
            this.bunifuLabel6.Name = "bunifuLabel6";
            this.bunifuLabel6.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel6.Size = new System.Drawing.Size(65, 30);
            this.bunifuLabel6.TabIndex = 26;
            this.bunifuLabel6.Text = "Ciudad:";
            this.bunifuLabel6.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel6.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // comboCiudadVinculada
            // 
            this.comboCiudadVinculada.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboCiudadVinculada.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboCiudadVinculada.FormattingEnabled = true;
            this.comboCiudadVinculada.Location = new System.Drawing.Point(121, 46);
            this.comboCiudadVinculada.Name = "comboCiudadVinculada";
            this.comboCiudadVinculada.Size = new System.Drawing.Size(215, 36);
            this.comboCiudadVinculada.TabIndex = 25;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label3.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3, 3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 30);
            this.label3.TabIndex = 24;
            this.label3.Text = " 3 ";
            // 
            // btnCrearComuna
            // 
            this.btnCrearComuna.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCrearComuna.Location = new System.Drawing.Point(91, 133);
            this.btnCrearComuna.Name = "btnCrearComuna";
            this.btnCrearComuna.Size = new System.Drawing.Size(215, 49);
            this.btnCrearComuna.TabIndex = 19;
            this.btnCrearComuna.Text = "Crear comuna";
            this.btnCrearComuna.UseVisualStyleBackColor = true;
            this.btnCrearComuna.Click += new System.EventHandler(this.btnCrearComuna_Click);
            // 
            // txtNombre
            // 
            this.txtNombre.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNombre.Location = new System.Drawing.Point(491, 347);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(200, 31);
            this.txtNombre.TabIndex = 22;
            // 
            // bunifuLabel7
            // 
            this.bunifuLabel7.AutoEllipsis = false;
            this.bunifuLabel7.CursorType = null;
            this.bunifuLabel7.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel7.Location = new System.Drawing.Point(372, 348);
            this.bunifuLabel7.Name = "bunifuLabel7";
            this.bunifuLabel7.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel7.Size = new System.Drawing.Size(113, 30);
            this.bunifuLabel7.TabIndex = 27;
            this.bunifuLabel7.Text = "Nombre sede:";
            this.bunifuLabel7.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel7.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // comboComunaVinculada
            // 
            this.comboComunaVinculada.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboComunaVinculada.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboComunaVinculada.FormattingEnabled = true;
            this.comboComunaVinculada.Location = new System.Drawing.Point(491, 303);
            this.comboComunaVinculada.Name = "comboComunaVinculada";
            this.comboComunaVinculada.Size = new System.Drawing.Size(200, 36);
            this.comboComunaVinculada.TabIndex = 29;
            // 
            // bunifuLabel8
            // 
            this.bunifuLabel8.AutoEllipsis = false;
            this.bunifuLabel8.CursorType = null;
            this.bunifuLabel8.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel8.Location = new System.Drawing.Point(410, 309);
            this.bunifuLabel8.Name = "bunifuLabel8";
            this.bunifuLabel8.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel8.Size = new System.Drawing.Size(75, 30);
            this.bunifuLabel8.TabIndex = 29;
            this.bunifuLabel8.Text = "Comuna:";
            this.bunifuLabel8.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel8.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // controlSede
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnActualizar);
            this.Controls.Add(this.bunifuLabel8);
            this.Controls.Add(this.comboComunaVinculada);
            this.Controls.Add(this.bunifuLabel7);
            this.Controls.Add(this.txtNombre);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.bunifuLabel1);
            this.Controls.Add(this.btnCrear);
            this.Controls.Add(this.panel1);
            this.Name = "controlSede";
            this.Size = new System.Drawing.Size(795, 535);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel1;
        private System.Windows.Forms.Button btnCrear;
        private System.Windows.Forms.ComboBox comboRegion;
        private System.Windows.Forms.Panel panel1;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel3;
        private System.Windows.Forms.ComboBox comboComunas;
        private System.Windows.Forms.ComboBox comboCiudad;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel2;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel4;
        private System.Windows.Forms.Button btnCrearRegion;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ComboBox comboRegionVinculada;
        private System.Windows.Forms.Label label2;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel5;
        private System.Windows.Forms.Button btnCrearCiudad;
        private System.Windows.Forms.Panel panel3;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel6;
        private System.Windows.Forms.ComboBox comboCiudadVinculada;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnCrearComuna;
        private System.Windows.Forms.TextBox txtNombre;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel7;
        private System.Windows.Forms.Button btnActualizar;
        private System.Windows.Forms.ComboBox comboComunaVinculada;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel8;
    }
}
