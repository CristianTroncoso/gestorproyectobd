﻿
namespace Proyecto.Views.UserForms
{
    partial class controlTrabajadores
    {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.bunifuLabel3 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel2 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel1 = new Bunifu.UI.WinForms.BunifuLabel();
            this.comboRut = new System.Windows.Forms.ComboBox();
            this.comboAcceso = new System.Windows.Forms.ComboBox();
            this.bunifuLabel4 = new Bunifu.UI.WinForms.BunifuLabel();
            this.comboJornada = new System.Windows.Forms.ComboBox();
            this.btnAsignar = new System.Windows.Forms.Button();
            this.bunifuLabel5 = new Bunifu.UI.WinForms.BunifuLabel();
            this.comboSede = new System.Windows.Forms.ComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.bunifuLabel6 = new Bunifu.UI.WinForms.BunifuLabel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.bunifuLabel7 = new Bunifu.UI.WinForms.BunifuLabel();
            this.btnAsignarSede = new System.Windows.Forms.Button();
            this.bunifuLabel8 = new Bunifu.UI.WinForms.BunifuLabel();
            this.comboRut2 = new System.Windows.Forms.ComboBox();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // bunifuLabel3
            // 
            this.bunifuLabel3.AutoEllipsis = false;
            this.bunifuLabel3.CursorType = null;
            this.bunifuLabel3.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel3.Location = new System.Drawing.Point(18, 74);
            this.bunifuLabel3.Name = "bunifuLabel3";
            this.bunifuLabel3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel3.Size = new System.Drawing.Size(132, 30);
            this.bunifuLabel3.TabIndex = 11;
            this.bunifuLabel3.Text = "Nivel de acceso:";
            this.bunifuLabel3.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel3.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // bunifuLabel2
            // 
            this.bunifuLabel2.AutoEllipsis = false;
            this.bunifuLabel2.CursorType = null;
            this.bunifuLabel2.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel2.Location = new System.Drawing.Point(118, 115);
            this.bunifuLabel2.Name = "bunifuLabel2";
            this.bunifuLabel2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel2.Size = new System.Drawing.Size(32, 30);
            this.bunifuLabel2.TabIndex = 10;
            this.bunifuLabel2.Text = "Rut:";
            this.bunifuLabel2.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel2.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // bunifuLabel1
            // 
            this.bunifuLabel1.AutoEllipsis = false;
            this.bunifuLabel1.CursorType = null;
            this.bunifuLabel1.Font = new System.Drawing.Font("Poppins", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel1.Location = new System.Drawing.Point(243, 23);
            this.bunifuLabel1.Name = "bunifuLabel1";
            this.bunifuLabel1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel1.Size = new System.Drawing.Size(311, 60);
            this.bunifuLabel1.TabIndex = 8;
            this.bunifuLabel1.Text = "Asignar trabajador";
            this.bunifuLabel1.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel1.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // comboRut
            // 
            this.comboRut.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboRut.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboRut.FormattingEnabled = true;
            this.comboRut.Location = new System.Drawing.Point(156, 112);
            this.comboRut.Name = "comboRut";
            this.comboRut.Size = new System.Drawing.Size(200, 36);
            this.comboRut.TabIndex = 13;
            // 
            // comboAcceso
            // 
            this.comboAcceso.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboAcceso.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboAcceso.FormattingEnabled = true;
            this.comboAcceso.Items.AddRange(new object[] {
            "Guia",
            "Administrador"});
            this.comboAcceso.Location = new System.Drawing.Point(156, 70);
            this.comboAcceso.Name = "comboAcceso";
            this.comboAcceso.Size = new System.Drawing.Size(200, 36);
            this.comboAcceso.TabIndex = 14;
            // 
            // bunifuLabel4
            // 
            this.bunifuLabel4.AutoEllipsis = false;
            this.bunifuLabel4.CursorType = null;
            this.bunifuLabel4.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel4.Location = new System.Drawing.Point(58, 158);
            this.bunifuLabel4.Name = "bunifuLabel4";
            this.bunifuLabel4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel4.Size = new System.Drawing.Size(92, 30);
            this.bunifuLabel4.TabIndex = 15;
            this.bunifuLabel4.Text = "ID Jornada:";
            this.bunifuLabel4.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel4.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // comboJornada
            // 
            this.comboJornada.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboJornada.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboJornada.FormattingEnabled = true;
            this.comboJornada.Location = new System.Drawing.Point(156, 155);
            this.comboJornada.Name = "comboJornada";
            this.comboJornada.Size = new System.Drawing.Size(200, 36);
            this.comboJornada.TabIndex = 16;
            // 
            // btnAsignar
            // 
            this.btnAsignar.BackColor = System.Drawing.Color.PaleGreen;
            this.btnAsignar.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAsignar.Location = new System.Drawing.Point(86, 212);
            this.btnAsignar.Name = "btnAsignar";
            this.btnAsignar.Size = new System.Drawing.Size(230, 60);
            this.btnAsignar.TabIndex = 17;
            this.btnAsignar.Text = "Asignar trabajador";
            this.btnAsignar.UseVisualStyleBackColor = false;
            this.btnAsignar.Click += new System.EventHandler(this.btnAsignar_Click);
            // 
            // bunifuLabel5
            // 
            this.bunifuLabel5.AutoEllipsis = false;
            this.bunifuLabel5.CursorType = null;
            this.bunifuLabel5.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel5.Location = new System.Drawing.Point(31, 116);
            this.bunifuLabel5.Name = "bunifuLabel5";
            this.bunifuLabel5.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel5.Size = new System.Drawing.Size(46, 30);
            this.bunifuLabel5.TabIndex = 18;
            this.bunifuLabel5.Text = "Sede:";
            this.bunifuLabel5.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel5.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // comboSede
            // 
            this.comboSede.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboSede.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboSede.FormattingEnabled = true;
            this.comboSede.Location = new System.Drawing.Point(83, 112);
            this.comboSede.Name = "comboSede";
            this.comboSede.Size = new System.Drawing.Size(200, 36);
            this.comboSede.TabIndex = 19;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.bunifuLabel6);
            this.panel1.Controls.Add(this.comboAcceso);
            this.panel1.Controls.Add(this.bunifuLabel2);
            this.panel1.Controls.Add(this.bunifuLabel3);
            this.panel1.Controls.Add(this.btnAsignar);
            this.panel1.Controls.Add(this.comboRut);
            this.panel1.Controls.Add(this.comboJornada);
            this.panel1.Controls.Add(this.bunifuLabel4);
            this.panel1.Location = new System.Drawing.Point(18, 89);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(379, 361);
            this.panel1.TabIndex = 20;
            // 
            // bunifuLabel6
            // 
            this.bunifuLabel6.AutoEllipsis = false;
            this.bunifuLabel6.CursorType = null;
            this.bunifuLabel6.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel6.Location = new System.Drawing.Point(118, 16);
            this.bunifuLabel6.Name = "bunifuLabel6";
            this.bunifuLabel6.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel6.Size = new System.Drawing.Size(131, 30);
            this.bunifuLabel6.TabIndex = 21;
            this.bunifuLabel6.Text = "Asignar jornada";
            this.bunifuLabel6.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel6.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.bunifuLabel8);
            this.panel2.Controls.Add(this.btnAsignarSede);
            this.panel2.Controls.Add(this.comboRut2);
            this.panel2.Controls.Add(this.bunifuLabel7);
            this.panel2.Controls.Add(this.comboSede);
            this.panel2.Controls.Add(this.bunifuLabel5);
            this.panel2.Location = new System.Drawing.Point(418, 89);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(322, 360);
            this.panel2.TabIndex = 21;
            // 
            // bunifuLabel7
            // 
            this.bunifuLabel7.AutoEllipsis = false;
            this.bunifuLabel7.CursorType = null;
            this.bunifuLabel7.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel7.Location = new System.Drawing.Point(83, 16);
            this.bunifuLabel7.Name = "bunifuLabel7";
            this.bunifuLabel7.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel7.Size = new System.Drawing.Size(155, 30);
            this.bunifuLabel7.TabIndex = 22;
            this.bunifuLabel7.Text = "Asignar trabajador";
            this.bunifuLabel7.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel7.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // btnAsignarSede
            // 
            this.btnAsignarSede.BackColor = System.Drawing.Color.PaleGreen;
            this.btnAsignarSede.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAsignarSede.Location = new System.Drawing.Point(48, 212);
            this.btnAsignarSede.Name = "btnAsignarSede";
            this.btnAsignarSede.Size = new System.Drawing.Size(230, 60);
            this.btnAsignarSede.TabIndex = 22;
            this.btnAsignarSede.Text = "Asignar sede";
            this.btnAsignarSede.UseVisualStyleBackColor = false;
            this.btnAsignarSede.Click += new System.EventHandler(this.btnAsignarSede_Click);
            // 
            // bunifuLabel8
            // 
            this.bunifuLabel8.AutoEllipsis = false;
            this.bunifuLabel8.CursorType = null;
            this.bunifuLabel8.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel8.Location = new System.Drawing.Point(45, 71);
            this.bunifuLabel8.Name = "bunifuLabel8";
            this.bunifuLabel8.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel8.Size = new System.Drawing.Size(32, 30);
            this.bunifuLabel8.TabIndex = 22;
            this.bunifuLabel8.Text = "Rut:";
            this.bunifuLabel8.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel8.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // comboRut2
            // 
            this.comboRut2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboRut2.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboRut2.FormattingEnabled = true;
            this.comboRut2.Location = new System.Drawing.Point(83, 68);
            this.comboRut2.Name = "comboRut2";
            this.comboRut2.Size = new System.Drawing.Size(200, 36);
            this.comboRut2.TabIndex = 23;
            // 
            // controlTrabajadores
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.bunifuLabel1);
            this.Name = "controlTrabajadores";
            this.Size = new System.Drawing.Size(795, 535);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel3;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel2;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel1;
        private System.Windows.Forms.ComboBox comboRut;
        private System.Windows.Forms.ComboBox comboAcceso;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel4;
        private System.Windows.Forms.ComboBox comboJornada;
        private System.Windows.Forms.Button btnAsignar;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel5;
        private System.Windows.Forms.ComboBox comboSede;
        private System.Windows.Forms.Panel panel1;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel6;
        private System.Windows.Forms.Panel panel2;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel7;
        private System.Windows.Forms.Button btnAsignarSede;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel8;
        private System.Windows.Forms.ComboBox comboRut2;
    }
}
