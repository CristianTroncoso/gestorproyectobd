﻿using MySql.Data.MySqlClient;
using Proyecto.Functions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyecto.Views.UserForms
{
    public partial class controlSede : UserControl
    {
        private string cadena = string.Empty;
        public controlSede()
        {
            InitializeComponent();

            DBUtils bd = new DBUtils();
            cadena = bd.returnConnectionString();

            refrescarInformacion();
        }

        private void btnCrearRegion_Click(object sender, EventArgs e)
        {
            Utils util = new Utils();
            util.crearRegion(comboRegion);
        }
        private void refrescarInformacion()
        {
            comboRegionVinculada.Items.Clear();
            comboCiudadVinculada.Items.Clear();
            comboComunaVinculada.Items.Clear();

            Utils util = new Utils();
            util.cargarRegiones(comboRegionVinculada);
            util.cargarCiudades(comboCiudadVinculada);
            util.cargarComunas(comboComunaVinculada);

            if ( comboRegionVinculada.Items.Count >= 1)
            {
                comboRegionVinculada.SelectedIndex = 0;
            }
            if ( comboCiudadVinculada.Items.Count >= 1) 
            {
                comboCiudadVinculada.SelectedIndex = 0;
            }
            if (comboComunaVinculada.Items.Count >= 1)
            {
                comboComunaVinculada.SelectedIndex = 0;
            }
        }
         

        private void btnCrearCiudad_Click(object sender, EventArgs e)
        {
            Utils util = new Utils();
            int idRegion = util.getIdRegion(comboRegionVinculada.SelectedItem.ToString());
            util.crearCiudad(idRegion, comboCiudad);
        }

        private void btnCrearComuna_Click(object sender, EventArgs e)
        {
            Utils util = new Utils();
            int idCiudad = util.getIdCiudad(comboCiudadVinculada.SelectedItem.ToString());
            util.crearComunas(idCiudad, comboComunas);
        }

        private void btnCrear_Click(object sender, EventArgs e)
        {
            Utils util = new Utils();
            int idComuna = util.getIdComuna(comboComunaVinculada.SelectedItem.ToString());
            util.crearSede(idComuna, txtNombre.Text);
        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            refrescarInformacion();
        }
    }
}
