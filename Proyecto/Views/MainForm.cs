﻿using System;
using Proyecto.Functions;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using Proyecto.Views.UserForms;

namespace Proyecto.Views
{
    public partial class MainForm : Form
    {
        private string connectionString = string.Empty;
        private string email = string.Empty;
        private string pass = string.Empty;

        private string rut = string.Empty;
        private string nombre = string.Empty;
        private string cargo = string.Empty; 
        public MainForm(string mail, string password)
        {
            InitializeComponent();
            DBUtils db = new DBUtils();
            connectionString = db.returnConnectionString();

            email = mail;
            pass = password;
            cargarDatosUsuario();

            controlJornada cJ = new controlJornada();
            AddControl(cJ);
        }

        private void cargarDatosUsuario()
        {
            try
            {
                using (MySqlConnection conexionBD = new MySqlConnection(connectionString))
                {
                    conexionBD.Open();
                    using (MySqlCommand cmd = new MySqlCommand("SELECT * FROM personas WHERE email=@1 AND contrasena=@2", conexionBD))
                    {
                        cmd.Parameters.AddWithValue("@1", email);
                        cmd.Parameters.AddWithValue("@2", pass);
                        MySqlDataReader dataReader;

                        dataReader = cmd.ExecuteReader();
                        dataReader.Read();
                        if (dataReader.HasRows == true)
                        {
                            nombre = (dataReader["nombre"].ToString()) + " " + (dataReader["apellido"].ToString());

                            rut = dataReader["rut"].ToString();

                            conexionBD.Close();
                        }
                        conexionBD.Close();
                    }
                    conexionBD.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            labelNombre.Text = nombre;
            labelCorreo.Text = email;
        }
        private void AddControl(Control control)
        {
            if (contentPanel.Controls.Contains(control) == false)
            {
                foreach (IDisposable control2 in contentPanel.Controls)
                    control2.Dispose();
                GC.Collect();
                GC.WaitForPendingFinalizers();

                contentPanel.Controls.Clear();
                control.Size = new Size(contentPanel.Width - 1, contentPanel.Height - 1);
                control.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
                contentPanel.Controls.Add(control);
            }
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }

        private void btnCrearJornada_Click(object sender, EventArgs e)
        {
            controlJornada cJ = new controlJornada();
            AddControl(cJ);
        }

        private void btnAgregarTrabajador_Click(object sender, EventArgs e)
        {
            controlTrabajadores cT = new controlTrabajadores();
            AddControl(cT);
        }

        private void btnAgregarSede_Click(object sender, EventArgs e)
        {
            controlSede cS = new controlSede();
            AddControl(cS);
        }

        private void agregarTour_Click(object sender, EventArgs e)
        {
            controlTour cTour = new controlTour();
            AddControl(cTour);
        }

        private void agregarFormas_Click(object sender, EventArgs e)
        {
            controlPagos cP = new controlPagos();
            AddControl(cP);
        }
    }
}
